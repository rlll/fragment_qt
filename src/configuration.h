#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <QtXml>

#include <QIODevice>
#include <QMap>
#include <QObject>
#include <QString>

class Configuration
{
public:
    Configuration();

    virtual ~Configuration();

    bool parse(const QString &);

    QString lastError();

    bool hasWarning();
    QString lastWarning();
    QStringList allWarnings();

protected:
    QString xmlFile;
    QString type;
    QString errorText;
    bool warningFlag;
    QString warningText;
    QStringList allWarningTexts;

    virtual void clearData();
    virtual bool parseDocument(QDomElement);
};

#endif

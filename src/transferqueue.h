#ifndef TRANSFERQUEUE_H
#define TRANSFERQUEUE_H

#include <QMutexLocker>
#include <QPair>
#include <QList>
#include <QQueue>

template <class T>
class TransferQueue
{
public:
    inline TransferQueue() {}

    inline T dequeue(bool *isEmpty)
        {
            QMutexLocker locker(&mutex);
            *isEmpty = queue.isEmpty();
            if (*isEmpty)
            {
                T t;
                return t;
            }
            else
            {
                return queue.dequeue();
            }
        }

    inline QList <T> dequeueList(quint32 size, bool *isEmpty)
        {
            QMutexLocker locker(&mutex);
            *isEmpty = queue.isEmpty();
            if (*isEmpty)
            {
                QList <T> t;
                return t;
            }
            else
            {
                QList <T> t;
                for (int i = size; i > 0; i--)
                {
                    if (queue.isEmpty())
                    {
                        return t;
                    }
                    else
                    {
                        t.append(queue.dequeue());
                    }
                }
                return t;
            }
        }


    inline void enqueue(const T &t)
        {
            QMutexLocker locker(&mutex);
            queue.enqueue(t);
        }

    inline void push_front(const T &t)
        {
            QMutexLocker locker(&mutex);
            queue.push_front(t);
        }

    inline void enqueueList(const QList <T> &t)
        {
            QMutexLocker locker(&mutex);
            for (int i=0; i<t.size(); i++)
            {
                queue.enqueue(t.at(i));
            }
        }

    inline int size()
        {
            QMutexLocker locker(&mutex);
            return queue.size();
        }
    inline bool isEmpty()
        {
            QMutexLocker locker(&mutex);
            return queue.isEmpty();
        }

    inline bool contains(const T &t)
        {
            QMutexLocker locker(&mutex);
            return queue.contains(t);
        }

private:
    QQueue <T> queue;
    mutable QMutex mutex;
};

#endif

// TRANSFERQUEUE_H

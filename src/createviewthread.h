#ifndef CREATEVIEWTHREAD_H
#define CREATEVIEWTHREAD_H

#include <QObject>
#include <QSvgRenderer>
#include <QImage>
#include <QPainter>
#include <QDebug>
#include <QMutex>

#include "application.h"
#include "global.h"

class CreateViewThread : public QObject
{
    Q_OBJECT
public:
    explicit CreateViewThread(QObject *parent = 0);
    sizes getSizes(int aRendererWidth, int aRendererHeight,
                   int aWdgWidth, int aWdgHeight);

    QStringList activeTemplates;


signals:
    void readyPicture(QImage, bool, sizes, QHash <QString, QRectF>,
                      QHash <QString, QRectF>, QObject *);

public slots:    
    void slotCreatePicture(QString, QString, QString, int, int, QHash <QString, QString>, QStringList,
                           bool, QObject *);
    void slotActivatedTemplates(QStringList);

};

#endif // CREATEVIEWTHREAD_H

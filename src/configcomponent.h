#ifndef CONFIGCOMPONENT_H
#define CONFIGCOMPONENT_H

#include <QMap>

#include "configuration.h"
#include "eventlog.h"
#include "global.h"
#include "replacerule.h"

class PropertyDescription
{
public:
    PropertyDescription();

    QString alias;
    QString display;
    QString event;
    QString database;
    QString sound;
    QString group;
    QString eval;

    bool isTrigger;

    QList<QStringList> conditonValue;
    QList<QStringList> conditonLexems;    
    QStringList valueUsedVars;
    QStringList lexemsUsedVars;

    QStringList valueCS;
    QStringList lexemsCS;

    void copyWithPrefix(const QString &,const PropertyDescription &);
};

class TemplateDescription
{
public:
    TemplateDescription();

    QString extension;
    QString name;
    bool isPrimary;
    bool isBottom;
};

class EventDescription
{
public:
    EventDescription();

    int priority;
    QString style;
    QString decoration;
    QString handler;
};

class ConfigComponent : public Configuration
{
public:
    ConfigComponent();

    QString DefaultValue;

    QString name;                           //Имя компонента
    QString description;                    //Описание компонента

    QStringList includes;                   //Подключаемые компоненты

    QMap<QString,QString> dbGroups;        //Список групп переменных в БД с псевдонимами
    QMap<QString,EventDescription> events;

    QMap<QString,PropertyDescription> propertiesV2;
    QMap<QString,TemplateDescription> templatesV2;

    QMap<QString,ReplaceRule> displayReplaceRules;

    QMap<QString,QMap<QString,QString> > eventRules;
    QMap<QString,QString> eventRulesAlias;
    QMap<QString,QMap<QString,QString> > eventTypeRules;
    QMap<QString,QString> alterEventRule;
    QMap<QString,QString> alterEventTypeRule;

    QMap<QString,ReplaceRule> databaseReplaceRules;

    QMap<QString,QMap<QString,QString> > soundRules;

    QStringList commandsConditions;        //Список условий команд
    QList<QStringList> commandsAbonents;   //Список абонентов команд
    QList<QStringList> commandsProtocols;  //Список протоколов команд
    QList<QStringList> commandsMessages;   //Список сообщений команд
    QList<QList<QList<QPair<QString,QString> > > >commandsFrom; //Список переменных компонента, связанных с переменными команды
    QList<QList<QList<QPair<QString,QString> > > >commandsTo; //Список переменных компонента, связанных с переменными команды
    QList<QList<int> > commandsDelays;
    QList<QList<QStringList> > commandLexems;
    QList<QStringList> commandUsedVars;
    QList<QStringList> commandCS;

    QStringList targets;                   //Список имен абонентов
    QMap<QString,QStringList> protocols;   //Список протоколов сообщений
    QMap<QString,QStringList> messages;    //Список номеров сообщений
    QMap<QString,QList<QList<QPair<QString,QString> > > > targetsFrom; //Список переменных компонента, связанных с переменными сообщения
    QMap<QString,QList<QList<QPair<QString,QString> > > > targetsTo;   //Список переменных компонента, связанных с переменными сообщения
    QMap<QString,QList<quint64> > times;   //Список времен отсылки сообщений

    QStringList outsideProtocols;          //Список протоколов извне
    QStringList outsideMessages;           //Список номеров сообщений извне

    QStringList nameUserFunctions;                   // Список имен пользовательских функций
    QMap<QString, UserFunction> userFunctions;              // Список объектов пользовательских функций

    // аппаратный журнал
    QHash <QString, QStringList> hashDeviceListCategories;
    QHash <QString, QStringList> hashCategoryListDevices;

    QHash <QString, QStringList> hashCategoryListAfters;
    QHash <QString, QStringList> hashAfterListCategories;

    QHash <QString, QString> hashPropertyBacToDesription;

    //Наблюдаемые переменные
    QStringList stores;
    QHash <QString,int> storeInterval;


protected:
    void clearData();
    bool parseDocument(QDomElement);
};

#endif

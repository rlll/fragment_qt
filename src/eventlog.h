#ifndef EVENTLOG_H
#define EVENTLOG_H

#include <QObject>

#include "application.h"

struct EventProp
{
    QString name;
    QString category;
    QString after;
    QString color;
};

class EventLog : public QObject
{
    Q_OBJECT
public:
    EventLog(QObject *parent = 0);
    ~EventLog();

    //Период архивирования журнала событий (с)
    enum EventsPeriods {WEEK = 0, DAY = 1};
    //Типы событий
    enum EventsTypes {UNKNOWN = 0, USUAL = 1, ONORNORM = 2, FAULT = 3, OFF = 4, FAIL = 5};

    static int getSecsByPeriod(int);
    static EventLog::EventsTypes getTypeByString(const QString &);
};


#endif

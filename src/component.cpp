#include "component.h"

Component::Component(ConfigComponent aConfComponent,
                     QObject *parent) : QObject(parent)
{
    config = aConfComponent;
    count =0;
}

Component::~Component()
{
    delete changedData;
    delete changedEvent;
}

bool Component::init()
{    
    changedData = new TransferQueue <QHash<QString,QString> >();
    changedEvent = new TransferQueue <QList <EventProp> >();

    //-------------Имя компонента---------------
    name = config.name;
    //-------------Описание компонента----------------
    description = config.description;
    //-----------Инициализация переменных-------------
    QMapIterator<QString,PropertyDescription> propertyItr(config.propertiesV2);
    while (propertyItr.hasNext())
    {
        propertyItr.next();
        propertiesV2[propertyItr.key()] = config.DefaultValue;
        if (propertyItr.value().eval != "")
        {            
            Evaluation *ev = new Evaluation(propertyItr.value().conditonLexems,
                                            propertyItr.value().lexemsUsedVars,
                                            propertyItr.value().lexemsCS,
                                            &(config.userFunctions),this);
            connect(ev,SIGNAL(signalMessage(QString,QString)),
                    this,SIGNAL(signalMessage(QString,QString)));
            evalsV2[propertyItr.key()] = ev;            
        }
    }
    //-----------Инициализация по умолчанию-----------
    propertyItr.toFront();
    while (propertyItr.hasNext())
    {
        propertyItr.next();
        if (!propertyItr.value().conditonValue.isEmpty())
        {
            Evaluation ev(propertyItr.value().conditonValue,
                          propertyItr.value().valueUsedVars,
                          propertyItr.value().valueCS,
                          &(config.userFunctions),this);
            propertiesV2[propertyItr.key()] = ev.evaluate(&propertiesV2);
        }
        propertiesChanges[propertyItr.key()] = false;
    }    
    //-----------Выявление связей между переменными------------
    QHashIterator<QString,Evaluation*> evalItr(evalsV2);
    while (evalItr.hasNext())
    {
        evalItr.next();
        QStringList usedVars = evalItr.value()->getUsedVars();

        for (int i=0; i<usedVars.size(); i++)
        {
            if (!propDepsV2.contains(usedVars.value(i),evalItr.key()))
            {
                propDepsV2.insert(usedVars.value(i),evalItr.key());
            }
        }
    }    
    //----------------Поиск переменных по шаблонам и кеширование основных шаблонов-------------
    QMapIterator<QString,TemplateDescription> templateItr(config.templatesV2);
    while (templateItr.hasNext())
    {
        templateItr.next();
        QString tmp = App->getProjectPath()+"/templates/"+templateItr.key()+".tpl";
        if (QFile::exists(tmp))
        {
            QString type = templateItr.value().extension;
            templates << templateItr.key();
            viewsTypes << type;
            if (templateItr.value().isPrimary)
            {
                createView(tmp);
            }
        }
        QStringList tmpProps = hasTemplateProperties(templateItr.key(),&propertiesV2);
        for (int i=0; i<tmpProps.size(); i++)
        {
            templatesPropertiesV2.insert(templateItr.key(),tmpProps[i]);
        }
    }

    //-----------------Список объектов вычислений условий выполнения команд-----------------
    commandsConditions = new QList<Evaluation*>();
    for (int i=0; i<config.commandsConditions.size(); i++)
    {
        Evaluation *cond = new Evaluation(config.commandLexems.value(i),
                                          config.commandUsedVars.value(i),
                                          config.commandCS.value(i),
                                          &(config.userFunctions),this);
        commandsConditions->append(cond);
    }

    return true;
}

QString Component::lastError()
{
    return errorText;
}

QString Component::getValue(const QString & aProperty)
{
    return propertiesV2.value(aProperty);
}

void Component::setProperties(QStringList properties, QStringList values)
{
    for (int i=0; i<properties.size(); i++)
    {
        setProperty(properties.at(i),values.at(i));
    }
}

void Component::setProperty(QString property, QString value)
{
    if (propertiesV2.contains(property) && propertiesV2.value(property) != value)
    {
        propertiesV2[property] = value;
        propertiesChanges[property] = true;
        hasChanged = true;
    }
}

bool Component::makeData()
{
    QList <EventProp> eventProp;    

    releaseProperties();
    QStringList changed = propertiesChanges.keys(true);
    for (int i=0; i<changed.size(); i++)
    {
        QString property = changed[i];
        QString value = propertiesV2.value(changed[i]);
        if (config.propertiesV2[property].isTrigger)
        {
            if (!config.propertiesV2[property].conditonValue.isEmpty())
            {
                Evaluation ev(config.propertiesV2[property].conditonValue,
                              config.propertiesV2[property].valueUsedVars,
                              config.propertiesV2[property].valueCS,
                              &(config.userFunctions),this);
                propertiesV2[property] = ev.evaluate(&propertiesV2);
            }
            else
            {
                propertiesV2[property] = config.DefaultValue;
            }
        }
        QString event = config.propertiesV2[property].event;
        if (event != "")
        {
            if (config.eventRules.contains(event))
            {
                QMap<QString,QString> rules = config.eventRules.value(event);
                QMap<QString,QString> rulesTypes = config.eventTypeRules.value(event);
                EventProp prop;
                prop.name = config.propertiesV2[property].alias;
                prop.category = event;
                if (rules.contains(value))
                {                    
                    prop.color = rulesTypes[value];
                    prop.after = rules[value];
                }
                else
                {
                    prop.color = config.alterEventTypeRule.value(event);
                    prop.after = config.alterEventRule.value(event);
                }
                eventProp.append(prop);
            }
        }
        if (App->needSound())
        {
            QString sound = config.propertiesV2[property].sound;
            if (sound != "")
            {
                if (config.soundRules.contains(sound))
                {
                    QMap<QString,QString> rules = config.soundRules.value(sound);
                    if (rules.contains(value))
                    {
                        emit needSound(rules[value]);
                    }
                }
            }
        }
    }    
    if (changed.size() > 0)
    {        
        QHash<QString,QString> enChangedProperties;
        QHash<QString,QString> storesProperties;
        for (int i=0; i<changed.size(); i++)
        {
            enChangedProperties.insert(changed[i],propertiesV2.value(changed[i]));            
            if (config.stores.contains(QString("%1@%2").arg(this->name).arg(changed.at(i))))
            {
                storesProperties.insert(QString("%1@%2").arg(this->name).arg(changed[i]),propertiesV2.value(changed[i]));
            }
        }
        changedData->enqueue(enChangedProperties);

        if (storesProperties.size() > 0)
        {
            emit storeData(storesProperties);
        }

        emit dataChanged();        
        createViewsOnScreens(false);
    }
    if (eventProp.size() > 0)
    {
        changedEvent->enqueue(eventProp);
        emit eventChanged();
    }

    for (int i=0; i<changed.size(); i++)
    {
        propertiesChanges[changed[i]] = false;
    }
    return (changed.size() > 0);
}

void Component::slotUpdateView()
{
    if (hasChanged)
    {
        hasChanged = false;
        makeData();
    }
}

void Component::null()
{
    QMapIterator<QString,PropertyDescription> propertyItr(config.propertiesV2);
    while (propertyItr.hasNext())
    {
        propertyItr.next();
        if (!propertyItr.value().conditonValue.isEmpty())
        {
            Evaluation ev(propertyItr.value().conditonValue,
                          propertyItr.value().valueUsedVars,
                          propertyItr.value().valueCS,
                          &(config.userFunctions),this);
            propertiesV2[propertyItr.key()] = ev.evaluate(&propertiesV2);
        }
        else
        {
            propertiesV2[propertyItr.key()] = config.DefaultValue;
        }
    }

    emit dataNulled(name);
    createViewsOnScreens(false);
}

bool Component::hasTemplate(const QString & aTemplate)
{
    return templates.contains(aTemplate);
}

void Component::createViewByTemplate(const QString & aTemplate)
{
    QString tmp = App->getProjectPath()+"/templates/"+aTemplate+".tpl";
    int index = templates.indexOf(aTemplate);
    if (index != -1)
    {
        QString type = viewsTypes[index];
        QString name = config.templatesV2.value(aTemplate).name;
        bool primary = config.templatesV2.value(aTemplate).isPrimary;
        QString view = createView(aTemplate);
        emit viewCreated(aTemplate,view,name,type,primary);
    }
}

void Component::deleteBuffer(const QString & aTemplate)
{
    templatesCacheV2.remove(aTemplate);
    viewsCache.remove(aTemplate);
}

void Component::setActiveTemplates(QStringList aTemplates)
{
    activeTemplates = aTemplates;
    createViewsOnScreens(false);
}

//protected
void Component::releaseProperties()
{
    evaluate(propertiesChanges.keys(true));
    //Вычисление условий
    for (int i=0; i<commandsConditions->size(); i++)
    {
        QString result = commandsConditions->value(i)->evaluate(&propertiesV2);
        if (result.toInt() > 0)
        {
            for (int j=0; j<config.commandsAbonents.value(i).size(); j++)
            {
                emit command(config.commandsAbonents.value(i).value(j),
                             config.commandsProtocols.value(i).value(j),
                             config.commandsMessages.value(i).value(j),
                             config.commandsTo.value(i).value(j),
                             config.commandsFrom.value(i).value(j),
                             config.commandsDelays.value(i).value(j));

            }
        }
    }
}

void Component::evaluate(const QStringList & aProperties)
{
    for (int i=0; i<aProperties.size(); i++)
    {        
        if (evalsV2.contains(aProperties[i]))
        {
            QString result = evalsV2.value(aProperties[i])->evaluate(&propertiesV2);

            if (propertiesV2.value(aProperties[i]) != result)
            {
                propertiesV2[aProperties[i]] = result;
                propertiesChanges[aProperties[i]] = true;
            }
        }
        QStringList deps = propDepsV2.values(aProperties[i]);        
        evaluate(deps);
    }
}

QString Component::createView(const QString & aTemplate, QStringList /*aChanged*/)
{
    QHash <QString,QString> hashTextIdToValue;

    QString buffer;
    QString source;
    QMap<int,QString> tplIndexes;
    QHash<QString,QPair<int,int> > tplDiffs;
    if (templatesCacheV2.contains(aTemplate) && viewsCache.contains(aTemplate))
    {
        source = templatesCacheV2.value(aTemplate);
        tplIndexes = templatesIndexes.value(aTemplate);
        tplDiffs = templatesDiffs.value(aTemplate);
        buffer = source;
    }
    else
    {
        QString tpl = App->getProjectPath()+"/templates/"+aTemplate+".tpl";
        QFile inFile(tpl);
        if (inFile.open(QFile::ReadOnly))
        {
            QTextStream stream(&inFile);
            source = stream.readAll();
            inFile.close();
        }
        source = createSourceNormalId(source);
        createHashTextValueToGId(source);
        templatesCacheV2.insert(aTemplate,source);
        buffer = source;


        QRegExp exp("[\\$]"+Evaluation::varPattern()+"[\\$]");
        exp.setMinimal(true);
        int index = 0;
        while ((index = exp.indexIn(buffer,index+1)) != -1)
        {
            QString property = exp.cap(0);
            property.remove(0,1);
            property.chop(1);
            if (propertiesV2.contains(property))
            {
                tplIndexes.insert(index,property);
                tplDiffs.insert(property,qMakePair(0,0));
            }
        }
        templatesIndexes.insert(aTemplate,tplIndexes);
        templatesDiffs.insert(aTemplate,tplDiffs);
    }

    QMapIterator<int,QString> indexesItr(tplIndexes);
    int diff = 0;
    while (indexesItr.hasNext())
    {
        indexesItr.next();
        QString property = indexesItr.value();
        QString after;
        QString displayMode = config.propertiesV2[property].display;
        QString value = propertiesV2.value(property);
        if (config.displayReplaceRules.contains(displayMode))
        {
            after = config.displayReplaceRules[displayMode].
                         applyRule(property,
                                   config.propertiesV2[property].alias,
                                   value,&(config.userFunctions));
        }
        else
        {
            if (displayMode.toLower() == "value")
            {
                after = value;
            }
            else
            {
                after = "ERROR";
            }
        }
        buffer.replace(indexesItr.key()+diff,property.size()+2,after);
        diff += after.size() - property.size() - 2;
        tplDiffs.insert(property,qMakePair(diff,after.size()));

        // изменился ли текст кнопок
        if (hashTextValueToGId.contains(property))
        {
            hashTextIdToValue.insert(hashTextValueToGId[property],after);
        }
    }
    viewsCache.insert(aTemplate,buffer);
    if (hashTextIdToValue.size())
    {
        emit changedButtonText(aTemplate,hashTextIdToValue);
    }
    return buffer;
}

void Component::createHashTextValueToGId(QString source)
{
    if (source.isEmpty())
        return;

    QDomDocument document;
    QString errorMsg;
    int errorLine;
    int errorColumn;
    if (!document.setContent(source.toUtf8(),&errorMsg,&errorLine,&errorColumn))
    {
        qDebug()<<errorMsg + QObject::tr(" (строка: ") + QString("%1").arg(errorLine) +
                    QObject::tr(" столбец: ") + QString("%1").arg(errorColumn) + QObject::tr(")");
        return;
    }
    QDomElement root = document.documentElement();

    setTextValueByGId(&root);
}

void Component::setTextValueByGId(QDomNode *aRoot)
{
    QDomNode node = aRoot->firstChild();
    while (!node.isNull())
    {
        if (node.toElement().tagName() == "a")
        {
            QDomNode node_g = node.firstChild();
            while (!node_g.isNull())
            {
                if(node_g.toElement().tagName() == "g")
                {
                    QDomNode node_text = node_g.firstChild();
                    while (!node_text.isNull())
                    {
                        if (node_text.toElement().tagName() == "text" &&
                                node_text.toElement().hasAttribute("id") &&
                                node_text.hasChildNodes())
                        {
                            hashTextValueToGId.insert(node_text.toElement().text().trimmed().remove("$"),
                                                         node_g.toElement().attribute("id"));
                        }
                        node_text = node_text.nextSibling();
                    }
                }
                node_g = node_g.nextSibling();
            }
        }

        setTextValueByGId(&node);
        node = node.nextSibling();
    }
}


QString Component::createSourceNormalId(QString source)
{
    if (source.isEmpty())
        return QString();

    QDomDocument document;
    QString errorMsg;
    int errorLine;
    int errorColumn;
    if (!document.setContent(source.toUtf8(),&errorMsg,&errorLine,&errorColumn))
    {
        qDebug()<<errorMsg + QObject::tr(" (строка: ") + QString("%1").arg(errorLine) +
                    QObject::tr(" столбец: ") + QString("%1").arg(errorColumn) + QObject::tr(")");
        return QString();
    }
    QDomElement root = document.documentElement();

    setNormalId(&root);
    return root.ownerDocument().toString();
}

void Component::setNormalId(QDomNode *aRoot)
{
    QDomNode node = aRoot->firstChild();
    QDomNode nextNode;
    while (!node.isNull())
    {
        nextNode = node.nextSibling();
        if (node.toElement().hasAttribute("id") && node.toElement().hasAttribute("style"))
        {
            QRegExp exp("(?:fill\\s*\\:\\s*\\$\\s*)(\\w+)(?:\\s*\\$)");
            int pos = exp.indexIn(node.toElement().attributeNode("style").value());
            if (pos > -1) {
                node.toElement().removeAttribute("id");
                node.toElement().setAttribute("id",exp.cap(1));
            }
        }
        setNormalId(&node);
        node = nextNode;
    }
}

void Component::createViewsOnScreens(bool aForAll)
{
    QStringList templatesOnScreens = activeTemplates;
    if (aForAll)
    {
        for (int i=0; i<templatesOnScreens.size(); i++)
        {
            QString currentTemplate = templatesOnScreens.value(i);
            int index = templates.indexOf(currentTemplate);
            if (index != -1)
            {
                QString view = createView(currentTemplate);
                QString type = viewsTypes[index];
                QString name = config.templatesV2.value(currentTemplate).name;
                bool primary = config.templatesV2.value(currentTemplate).isPrimary;
                emit viewChanged(currentTemplate,view,name,type,primary,EventLog::OFF);
            }
        }
    }
    else
    {
        for (int i=0; i<templatesOnScreens.size(); i++)
        {
            QString currentTemplate = templatesOnScreens.value(i);
            int index = templates.indexOf(currentTemplate);
            if (index != -1)
            {
				QString view = createView(currentTemplate);
				QString type = viewsTypes[index];
				QString name = config.templatesV2.value(currentTemplate).name;
				bool primary = config.templatesV2.value(currentTemplate).isPrimary;
				EventLog::EventsTypes maxType = EventLog::UNKNOWN;
				emit viewChanged(currentTemplate,view,name,type,primary,(int)maxType);
            }
        }
    }
}

QStringList Component::hasTemplateProperties(const QString & aTemplate, QHash<QString, QString> *aProperties)
{
    QString tmp = App->getProjectPath()+"/templates/"+aTemplate+".tpl";
    QString buffer;
    QFile inFile(tmp);
    if (inFile.open(QFile::ReadOnly))
    {
        QTextStream stream(&inFile);
        buffer = stream.readAll();
        inFile.close();
    }

    QStringList tmpProperties;
    QRegExp exp("[\\$]"+Evaluation::varPattern()+"[\\$]");
    int index = 0;
    while ((index = exp.indexIn(buffer,index+1)) != -1)
    {
        QString property = exp.cap(0);
        property.remove(0,1);
        property.chop(1);
        tmpProperties << property;
    }
    QStringList coincidenceList;
    for (int i=0; i<tmpProperties.size(); i++)
    {
        if (aProperties->contains(tmpProperties.value(i)))
        {
            coincidenceList << tmpProperties.value(i);
        }
    }

    return coincidenceList;
}

QStringList Component::hasTemplatePropertiesFromCache(const QString & aTemplate)
{
    QStringList coincidenceList;
    QStringList props = templatesPropertiesV2.values(aTemplate);
    for (int i=0; i<props.size(); i++)
    {
        if (propertiesChanges.value(props[i]))
        {
            coincidenceList << props[i];
        }
    }
    return coincidenceList;
}

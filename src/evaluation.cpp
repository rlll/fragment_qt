#include "evaluation.h"

Evaluation::Evaluation(const QList <QStringList> aConditionLexems,
                       const QStringList aUsedVars,
                       const QStringList aConstStrings,
                       QMap<QString, UserFunction>* aUserFunctions,
                       QObject *parent) : QObject(parent)
{
    usedVars = aUsedVars;
    constStrings = aConstStrings;
    conditionLexems = aConditionLexems;
    userFunctions = aUserFunctions;    
    initSintax();
}


Evaluation::Evaluation(const QString & aExpression, QStringList aNameUserFunctions, QObject *parent) : QObject(parent)
{
    nameUserFunctions = aNameUserFunctions;
    initSintax();
    expression = deleteSpaces(aExpression);
    ruleForBrackets(expression);
    ruleForNonAccountDelimiters(expression);

    if (!expression.isEmpty())
    {
        preSplitOnLexems();
        if (errorText.isEmpty())
        {
            splitOnLexems();
        }
    }
}

Evaluation::~Evaluation()
{
}

void Evaluation::setExpression(const QString & aExpression)
{
    expression = aExpression;
    
    splitOnLexems();
}

QString Evaluation::getExpression()
{
    return expression;
}

QStringList Evaluation::getUsedVars()
{
    return usedVars;
}

//protected
void Evaluation::initSintax()
{
    operations << "*" << "+";
    operationList << &Evaluation::operationAnd << &Evaluation::operationOr;
    functions << "dep"
              << "equal" << "nonEqual"
              << "eq" << "neq"
              << "mult" << "sum"
              << "switch"
              << "more"  << "less"
              << "inRange"
              << "message"
              << "mid"
              << "mid64"
              << "strf"
              << "messageIf"
              << "mod"
              << "not";
    functionList << &Evaluation::functionDep
                 << &Evaluation::functionEqual << &Evaluation::functionNonEqual
                 << &Evaluation::functionEqual << &Evaluation::functionNonEqual
                 << &Evaluation::functionMult << &Evaluation::functionSum
                 << &Evaluation::functionSwitch
                 << &Evaluation::functionMore << &Evaluation::functionLess
                 << &Evaluation::functionInRange
                 << &Evaluation::functionMessage                 
                 << &Evaluation::functionMid
                 << &Evaluation::functionMid64
                 << &Evaluation::functionStrF
                 << &Evaluation::functionMessageIf
                 << &Evaluation::functionMod
                 << &Evaluation::functionNot;

    for (int i=0; i<functions.size(); i++)
    {
        functionDelimiters << functions[i] + "(";
    }

    for (int i=0; i<nameUserFunctions.size(); i++)
    {
        userFunctionDelimiters << nameUserFunctions[i] + "(";
    }

    functionDelimiters += userFunctionDelimiters;

    for (int i=0; i<operations.size(); i++)
    {
        operationDelimiters << operations[i];
    }

    conditionIf = "if";
    conditions << conditionIf;
    for (int i=0; i<conditions.size(); i++)
    {
        conditionDelimiters << conditions[i] + "(";
    }


    delimiters << "(" << ")" << ",";
    preDelimiters = delimiters + operationDelimiters;

    delimiters = preDelimiters + functionDelimiters + conditionDelimiters;

    priorityUpDelimiters << "(";
    priorityUpDelimiters += functionDelimiters + conditionDelimiters;

    priorityDownDelimiters << ")";

    nonAccountDelimiters << ",";   

    stringMark = "'";

    leftSeparatorDelimiter = "(";
    rightSeparatorDelimiter = ")";

    separatorDelimiters << leftSeparatorDelimiter
                        << rightSeparatorDelimiter;
}

void Evaluation::ruleForBrackets(QString source)
{
    for (int i=0; i<source.length(); i++)
    {
        if (QString(source.at(i)) == leftSeparatorDelimiter && i<source.length()-1)
        {
            if (nonAccountDelimiters.contains(QString(source.at(i+1))) ||
                operations.contains(QString(source.at(i+1))))
            {
                errorText = "Некоректная последовательность после '('. \nВыражение: " + expression;
                return;
            }
        }
        if (QString(source.at(i)) == rightSeparatorDelimiter && i>0)
        {
            if (nonAccountDelimiters.contains(QString(source.at(i-1))) ||
                operations.contains(QString(source.at(i-1))))
            {
                errorText = "Некоректная последовательность перед ')'. \nВыражение: " + expression;
                return;
            }
        }
        if (QString(source.at(i)) == rightSeparatorDelimiter && i<(source.length()-1))
        {
            if (!(rightSeparatorDelimiter == QString(source.at(i+1)) ||
                  operations.contains(QString(source.at(i+1))) ||
                  nonAccountDelimiters.contains(QString(source.at(i+1)))
                 )
               )
            {
                errorText = "Некоректная последовательность после ')'. \nВыражение: " + expression;
                return;
            }
        }
        if ( (i==0 || i == source.length()-1) && operations.contains(QString(source.at(i))) )
        {
            errorText = "Выражение заканчивается или начинается некорректно. \nВыражение: " + expression;
            return;
        }
        if (i==0 && rightSeparatorDelimiter == QString(source.at(i)) )
        {
            errorText = "Выражение начинается некорректно. \nВыражение: " + expression;
            return;
        }
    }
}

void Evaluation::ruleForNonAccountDelimiters(QString source)
{
    for (int i=0; i<source.length(); i++)
    {
        if (nonAccountDelimiters.contains(QString(source.at(i))) && (i>0 || i<source.length()-1))
        {
            if ( operations.contains(QString(source.at(i+1))) ||
                 QString(source.at(i+1)) == rightSeparatorDelimiter ||
                 nonAccountDelimiters.contains(QString(source.at(i+1)))
                )
            {
                errorText = "Некоректная последовательность после ','. \nВыражение: " + expression;
                return;
            }
            if ( operations.contains(QString(source.at(i-1))) ||
                 QString(source.at(i-1)) == leftSeparatorDelimiter ||
                 nonAccountDelimiters.contains(QString(source.at(i-1)))
                )
            {
                errorText = "Некоректная последовательность перед ','. \nВыражение: " + expression;
                return;
            }

        }
    }
}

QString Evaluation::deleteSpaces(QString source)
{    
    QString result = source;
    QStringList pieces;
    int index=-1;

    while ( (index=result.indexOf(QRegExp("(\\s|"+QRegExp::escape(stringMark)+")"))) != -1)
    {
        QString str2;
        if (stringMark == result.at(index))
        {
            int index2 = result.indexOf(stringMark,index+1);
            str2 = result.mid(index+1,index2+1);
            result.remove(index+1,index2+1);
        }
        pieces << result.left(index+1).trimmed();
        result.remove(0,index+1);

        pieces<<str2;        
    }
    pieces << result;

    result.clear();
    for (int i=0; i<pieces.size(); i++)
    {
        QString str = pieces[i].trimmed();
        if (!str.isEmpty())
        {
            result += str;
        }
    }    
    return result;
}

void Evaluation::fullSplitOnLexems()
{
    QString source = expression;
    QStringList lexems;
    QRegExp reg(patternByDelimiters(preDelimiters));
    int index;
    QString lexem;
    while ((index = reg.indexIn(source)) != -1)
    {
        lexem += source.left(index).trimmed();
        if (lexem.contains(stringMark))
        {
            int indexMarkFirst = lexem.indexOf(stringMark);
            int indexMarkLast = lexem.indexOf(stringMark,indexMarkFirst+1);
            if (indexMarkLast == -1)
            {
                lexem += reg.cap(1);
                source.remove(0,index + reg.cap(1).size());
                if (source.size() == 0)
                {
                    errorText = "Не верное число `'`. \nВыражение: " + expression;
                    return;
                }
                continue;
            }
            if ( indexMarkLast+1 != lexem.size() )
            {
                errorText = "Недопустимые символы после закрывающей `'`. \nВыражение: " + expression;
                return;
            }
        }
        if (lexem != "")
        {
            lexems << lexem;
        }
        QString delemiter = reg.cap(1);
        lexems << delemiter;
        source.remove(0,index + delemiter.size());
        lexem.clear();
    }
    if (!source.isEmpty())
    {
        lexems << source.trimmed();
    }

    for (int i=0; i<lexems.size(); i++)
    {
        if (lexems.at(i) == leftSeparatorDelimiter && i > 0)
        {
            QString left = lexems.at(i-1);
        }
    }
}

void Evaluation::preSplitOnLexems()
{   
    QString source = expression;
    QStringList lexems;
    QRegExp reg(patternByDelimiters(preDelimiters));
    int index;
    QString lexem;
    while ((index = reg.indexIn(source)) != -1)
    {
        lexem += source.left(index).trimmed();
        if (lexem.contains(stringMark))
        {
            int indexMarkFirst = lexem.indexOf(stringMark);
            int indexMarkLast = lexem.indexOf(stringMark,indexMarkFirst+1);
            if (indexMarkLast == -1)
            {
                lexem += reg.cap(1);
                source.remove(0,index + reg.cap(1).size());
                if (source.size() == 0)
                {
                    errorText = "Не верное число `'`. \nВыражение: " + expression;
                    return;
                }
                continue;
            }
            if ( indexMarkLast+1 != lexem.size() )
            {
                errorText = "Недопустимые символы после закрывающей `'`. \nВыражение: " + expression;
                return;
            }
        }
        if (lexem != "")
        {
            lexems << lexem;
        }
        QString delemiter = reg.cap(1);
        if (!nonAccountDelimiters.contains(delemiter))
        {
            lexems << delemiter;
        }
        source.remove(0,index + delemiter.size());
        lexem.clear();
    }

    if (!source.isEmpty())
    {
        lexems << source.trimmed();
    }

    // Проверка парности скобок

    int countBrackets=0;
    // Проверка синтаксиса перед открывающейся скобкой
    for (int i=0; i<lexems.size(); i++)
    {
        if (lexems.at(i) == rightSeparatorDelimiter)
        {
            countBrackets--;
        }
        else if (lexems.at(i) == leftSeparatorDelimiter)
        {
            countBrackets++;
            if (i>0)
            {
                QString left = lexems.at(i-1);
                if (left == rightSeparatorDelimiter)
                {
                    errorText = "Некоректный порядок скобок. \nВыражение: " + expression;
                    return;
                }
                if (!functions.contains(left) &&
                    !nonAccountDelimiters.contains(left) &&
                    !operations.contains(left) &&
                    left != leftSeparatorDelimiter &&
                    !nameUserFunctions.contains(left))
                {                    
                    errorText = "Недопустимая комбинация перед '(' :"+lexems.at(i-1)+".\nВыражение: " + expression;
                    return;
                }
            }
        }

        if (countBrackets<0)
        {
            errorText = "Некоректный порядок скобок. \nВыражение: " + expression;
            return;
        }
    }    
    if (countBrackets!=0)
    {
        errorText = "Не верное число скобок. \nВыражение: " + expression;
        return;
    }    
}

QList <int> Evaluation::getPriorities(QStringList aLexems)
{
    QList<int> priorities;
    QString priorityUpPattern = patternByDelimiters(priorityUpDelimiters);
    QString priorityDownPattern = patternByDelimiters(priorityDownDelimiters);
    int priority = 0;
    for (int i=0; i<aLexems.size(); i++)
    {
        if (QRegExp(priorityUpPattern).exactMatch(aLexems[i]))
        {
            priority++;
            priorities << priority;
        }
        else if (QRegExp(priorityDownPattern).exactMatch(aLexems[i]))
        {
            priorities << priority;
            priority--;
        }
        else
        {
            priorities << priority;
        }
    }

    return priorities;
}

void Evaluation::getCondTree(QList <QStringList> *aCondTree, QStringList aLexems, QList <int> aPriorities, int aStart)
{
    int index = aCondTree->size();
    aCondTree->append(QStringList());

    QRegExp condExp(patternByDelimiters(conditionDelimiters));
    QString priorityDownPattern = patternByDelimiters(priorityDownDelimiters);

    QStringList lexems;
    if (aStart!=0)
    {
        lexems <<aLexems[0];
    }
    int fromPriority=0;
    for (int i=aStart; i<aLexems.size(); )
    {
        if (condExp.exactMatch(aLexems[i]))
        {
            fromPriority = aPriorities[i];
            QStringList bufLexems;
            bufLexems << aLexems[i];
            i++;
            while (i<aLexems.size())
            {
                bufLexems << aLexems[i];
                if (aPriorities[i] == fromPriority &&
                    QRegExp(priorityDownPattern).exactMatch(aLexems[i]))
                {
                    i++; // пропуск закрывающей скобки
                    break;
                }
                i++;
            }
            lexems << QString("t:%1").arg(aCondTree->size());
            getCondTree (aCondTree,bufLexems,getPriorities(bufLexems),1);
        }
        else
        {
            lexems << aLexems[i];
            i++;
        }
    }
    aCondTree->replace(index,lexems);
}

void Evaluation::splitOnLexems()
{
    QString source = expression;
    QStringList lexems;
    QRegExp reg(patternByDelimiters(delimiters));
    int index;
    QString lexem;
    while ((index = reg.indexIn(source)) != -1)
    {
        lexem += source.left(index).trimmed();
        if (lexem.contains(stringMark))
        {
            int indexMarkFirst = lexem.indexOf(stringMark);
            int indexMarkLast = lexem.indexOf(stringMark,indexMarkFirst+1);
            if (indexMarkLast == -1)
            { 
                lexem += reg.cap(1);                
                source.remove(0,index + reg.cap(1).size());
                continue;
            }
        }
        if (lexem != "")
        {
            lexems << lexem;
        }
        QString delemiter = reg.cap(1);        
        if (!nonAccountDelimiters.contains(delemiter))
        {
            lexems << delemiter;
        }
        source.remove(0,index + delemiter.size());
        lexem.clear();
    }    
    if (!source.isEmpty())
    {
        lexems << source.trimmed();
    }

    QList<int> priorities = getPriorities(lexems);

    QList <QStringList> condTree;
    getCondTree(&condTree,lexems,priorities);
    for (int k=0; k<condTree.size(); k++)
    {
        readyLexems.clear();
        lexems = condTree[k];
        if (lexems.size() == 1)
        {            
            conditionLexems << (QStringList()<<lexemWithType(lexems[0]));
            continue;
        }

        priorities = getPriorities(lexems);
        while (lexems.size() > 1)
        {
            int maxPriority = 0;
            for (int i=0; i<priorities.size(); i++)
            {
                if (priorities[i] > maxPriority)
                {
                    maxPriority = priorities[i];
                }
            }
            QStringList prSimpleLexems;
            int index = priorities.indexOf(maxPriority);
            int j = priorities.size();
            for (int i=index; i<j; i++)
            {
                prSimpleLexems << lexems[index];
                if (lexems[index] == ")")
                {
                    lexems.removeAt(index);
                    priorities.removeAt(index);
                    break;
                }
                lexems.removeAt(index);
                priorities.removeAt(index);
            }

            QStringList simpleLexems;
            for (int i=0; i<prSimpleLexems.size(); i++)
            {
                if (!QRegExp(patternByDelimiters(separatorDelimiters)).exactMatch(prSimpleLexems[i]))
                {
                    simpleLexems << prSimpleLexems[i];
                }
            }

            QRegExp opExp(patternByDelimiters(operationDelimiters));
            QRegExp opFuncExp(patternByDelimiters(operationDelimiters+functionDelimiters));
            QRegExp funcExp(patternByDelimiters(functionDelimiters));
            QRegExp userFuncExp(patternByDelimiters(userFunctionDelimiters));
            QRegExp condExp(patternByDelimiters(conditionDelimiters));

            int indexOperation;
            if ((indexOperation = simpleLexems.indexOf(opExp)) != -1)
            {
                for (int i=0; i<operationDelimiters.size(); i++)
                {
                    int j;
                    while ((j = simpleLexems.indexOf(operationDelimiters[i])) != -1)
                    {
                        readyLexems << "o:"+operationDelimiters[i]+","+
                                       lexemWithType(simpleLexems[j-1])+","+
                                       lexemWithType(simpleLexems[j+1]);
                        simpleLexems.removeAt(j-1);
                        simpleLexems.removeAt(j-1);
                        simpleLexems.replace(j-1,QString("l:%1").arg(readyLexems.size()-1));
                    }
                }
            }

            int indexCond = simpleLexems.indexOf(condExp);
            if (indexCond != -1)
            {
                QString readyLexem;
                QString condFunc = simpleLexems[indexCond];
                condFunc.chop(1);
                readyLexem = QString("z:%1").arg(condFunc);
                int i=1;
                for (; i<simpleLexems.size(); i++)
                {
                    readyLexem += ","+lexemWithType(simpleLexems[i]);
                }
                if (!(i%2))
                {
                    errorText = QString("Функция условия некорректна. Число параметров должно быть четным."
                                        "\nВыражение:\n\t%1").arg(expression);
                    return;
                }
                readyLexems.push_front(readyLexem);
                break;
            }

            int indexOpFunc = simpleLexems.indexOf(opFuncExp);
            if (indexOpFunc != -1)
            {
                QString readyLexem;
                QString opFunc = simpleLexems[indexOpFunc];
                if (opExp.exactMatch(opFunc))
                {
                    readyLexem = "o:" + opFunc + ",";
                }
                else if (userFuncExp.exactMatch(opFunc))
                {
                    opFunc.chop(1);
                    readyLexem = "u:" + opFunc + ",";
                }
                else if (funcExp.exactMatch(opFunc))
                {
                    opFunc.chop(1);
                    readyLexem = "f:" + opFunc + ",";
                }
                simpleLexems.removeAt(indexOpFunc);
                for (int i=0; i<simpleLexems.size(); i++)
                {
                    readyLexem += lexemWithType(simpleLexems[i]) + ((i != simpleLexems.size()-1)?",":"");
                }
                readyLexems << readyLexem;
            }
            else
            {
                indexOpFunc = simpleLexems.indexOf(QRegExp("[l][:][0-9]+"));
                if (indexOpFunc == -1)
                {                    
                    errorText = "Неизвестная операция.\nВыражение:" + expression;
                    readyLexems.clear();
                    return;
                }
            }
            lexems.insert(index,QString("l:%1").arg(readyLexems.size()-1));
            priorities.insert(index,maxPriority-1);
        }
        if (lexems.size() == 1)
        {
            readyLexems << lexems[0];
        }
        conditionLexems << readyLexems;
    }
    for (int i=1; i<conditionLexems.size(); i++)
    {
        QStringList lexems = conditionLexems[i];
        if (lexems.size()==0)
        {
            errorText = "Ошибка программы Evaluate::splitOnLexems():1";
            return;
        }
        QStringList firstLexem = lexems[0].split(",");
        lexems.removeFirst();
        if (firstLexem.size()==0)
        {
            errorText = "Ошибка программы Evaluate::splitOnLexems():2";
            return;
        }
        QString cond = firstLexem[0].split(":").at(1);
        if (cond == conditionIf)
        {
            QStringList readyList;
            for (int j=1; j<firstLexem.size();j++)
            {
                readyList << getOrder(firstLexem[j],lexems);
            }
            readyList.push_front(QString("z:%1").arg(cond));
            conditionLexems[i] = readyList;
        }
    }
}

QString Evaluation::getOrder(QString aFirstLexem, QStringList lexems)
{
    QString result;
    QStringList stack;
    QStringList indexes;
    QStringList splitFirstLexem = aFirstLexem.split(":");
    if (splitFirstLexem.at(0) == "l")
    {
        stack << splitFirstLexem.at(1);
    }
    else
    {
        return aFirstLexem;
    }

    while (!stack.isEmpty())
    {
        QString index = stack.takeFirst();
        indexes << index;
        if (lexems.size() <= index.toInt())
        {
            errorText = "Ошибка программы Evaluate::getOrder():1";
            return "";
        }
        QStringList splitLexem = lexems.at(index.toInt()).split(",");
        for (int i=0; i<splitLexem.size(); i++)
        {
            QStringList list = splitLexem.at(i).split(":");
            if (list.at(0) == "l")
            {
                stack << list.at(1);
            }
        }
    }
    QStringList bufIndexes = sort(indexes);

    QStringList resultLexems;
    for (int i=0; i<bufIndexes.size(); i++)
    {
        resultLexems << lexems.at(bufIndexes.at(i).toInt());
    }

    // Получение всех индексов l:
    QStringList inds;
    for (int i=0; i<resultLexems.size(); i++)
    {
        QStringList split = resultLexems.at(i).split(",");
        for (int j=0; j<split.size(); j++)
        {
            QStringList list = split.at(j).split(":");
            if (list.at(0) == "l")
            {
                inds << list.at(1);
            }
        }
    }
    inds = sort(inds);

    for (int i=0; i<resultLexems.size(); i++)
    {
        QStringList split = resultLexems.at(i).split(",");
        for (int j=0; j<split.size(); j++)
        {
            QStringList list = split.at(j).split(":");
            if (list.at(0) == "l")
            {
                int ind = inds.indexOf(list.at(1));
                split.replace(j,QString("%1:%2").arg(list.at(0)).arg(ind));
            }
        }
        QString res;
        for (int j=0; j<split.size(); j++)
        {
            res += split.at(j) + + ((j != split.size()-1)?",":"");
        }
        resultLexems.replace(i,res);
    }

    for (int i=0; i<resultLexems.size(); i++)
    {
        result += resultLexems.at(i) + ((i != resultLexems.size()-1)?"▬":""); // alt+22
    }
    return result;
}

QStringList Evaluation::sort(QStringList indexes)
{
    QStringList bufIndexes;
    while (indexes.size()!=0)
    {
        QString min = indexes.at(0);
        for (int i=1; i<indexes.size(); i++)
        {
            if (min.toInt() > indexes.at(1).toInt())
            {
                min = indexes.at(1);
            }
        }
        indexes.removeAll(min);
        if (!bufIndexes.contains(min))
        {
            bufIndexes << min;
        }
    }
    return bufIndexes;
}

QString Evaluation::evaluate(QStringList aReadyLexems, QHash<QString,QString> *aContext)
{    
    QStringList values;
    for (int i=0; i<aReadyLexems.size(); i++)
    {
        QStringList parts = aReadyLexems[i].split(",");
        QStringList actionParts = parts[0].split(":");
        parts.removeAt(0);

        QStringList parameterValues;
        for (int j=0; j<parts.size(); j++)
        {
            QStringList parameterParts = parts[j].split(":");
            if (parameterParts[0] == "v")
            {
                parameterValues << aContext->value(parameterParts[1]);
            }
            else if ((parameterParts[0] == "ci") || (parameterParts[0] == "cf"))
            {
                parameterValues << parameterParts[1];
            }
            else if (parameterParts[0] == "cs")
            {
                bool ok;
                int ind = parameterParts[1].toInt(&ok);
                if (ok)
                {
                    parameterValues << constStrings[ind];
                }
                else
                {
                    QMessageBox message;
                    message.setInformativeText(tr("Получен не корректный индекс constStrings в Evaluation::evaluate().\n"));
                    message.exec();
                    parameterValues <<"";
                }
            }
            else if (parameterParts[0] == "l")
            {
                parameterValues << values[parameterParts[1].toInt()];
            }
            else if (parameterParts[0] == "t")
            {
                parameterValues << evaluateT(parameterParts[1].toInt(),aContext);
            }
        }

        if (actionParts[0] == "v")
        {
            values << aContext->value(actionParts[1]);
        }
        else if ((actionParts[0] == "ci") || (actionParts[0] == "cf") || (actionParts[0] == "cs"))
        {
            values << actionParts[1];
        }
        else if (actionParts[0] == "l")
        {
            values << values[actionParts[1].toInt()];
        }
        else if (actionParts[0] == "o")
        {
            int indexOperation = operations.indexOf(actionParts[1]);
            if (indexOperation != -1)
            {
                Operation opPtr = operationList[indexOperation];
                values << (this->*opPtr)(parameterValues);
            }
            else
            {
                values << "";
            }
        }
        else if (actionParts[0] == "u")
        {
            if (userFunctions->contains(actionParts[1]))
            {                
                UserFunction uF = userFunctions->value(actionParts.at(1));
                values << evaluateU(uF,parameterValues,aContext);
            }
            else
            {
                values << "";
            }
        }
        else if (actionParts[0] == "f")
        {
            int indexFunction = functions.indexOf(actionParts[1]);
            if (indexFunction != -1)
            {
                Function funcPtr = functionList[indexFunction];
                values << (this->*funcPtr)(parameterValues);
            }
            else
            {
                values << "";
            }
        }
        else if (actionParts[0] == "t")
        {
            values << evaluateT(actionParts[1].toInt(),aContext);
        }
        else
        {
            values << "";
        }
    }

    if (values.size() > 0)
    {
        return values[values.size()-1];
    }
    else
    {
        return "";
    }
}

QString Evaluation::evaluateU(UserFunction aUF, QStringList aParameterValues, QHash<QString,QString> *aContext)
{
    if (aUF.arguments.size() != aParameterValues.size())
    {
        qDebug()<<"Не верное число параметров функции:"+aUF.name;
        return "";
    }    
    QStringList listArgs = aUF.arguments;
    QHash<QString,QString> hashContext;
    for (int i=0; i<listArgs.size();i++)
    {
        hashContext[listArgs.at(i)] = aParameterValues.at(i);        
    }
    Evaluation ev(aUF.conditionLexems,
                  aUF.usedVars,
                  aUF.cS,
                  userFunctions);
    return ev.evaluate(&hashContext);
}

QString Evaluation::evaluateT(int level, QHash<QString,QString> *aContext)
{
    QStringList lexems = conditionLexems[level];
    QString action = lexems[0].split(":").at(0);
    QString cond = lexems[0].split(":").at(1);

    lexems.removeFirst();
    if (cond == conditionIf)
    {
        for (int i=0; i<lexems.size(); i=i+2)
        {
            QStringList splitLexems = lexems[i].split("▬");
            QString resultIf = evaluate(splitLexems,aContext);

            if (resultIf != "0" && !resultIf.isEmpty())
            {
                return evaluate(lexems[i+1].split("▬"),aContext);
            }
        }
    }
    return "";
}

QString Evaluation::evaluate(QHash<QString,QString> *aContext)
{            
    return evaluate(conditionLexems[0],aContext);
}

QString Evaluation::patternByDelimiters(const QStringList & aDelimiters)
{
    QString pattern = "(";
    for (int i=0; i<aDelimiters.size(); i++)
    {
        if (i != aDelimiters.size()-1)
        {
            pattern += "("+QRegExp::escape(aDelimiters[i]) + ")|";
        }
        else
        {
            pattern += "("+QRegExp::escape(aDelimiters[i]) + ")";
        }
    }
    return pattern += ")";
}

QString Evaluation::lexemWithType(const QString & aLexem)
{
    //# - ссылка на другое простое выражение
    //c[i|f|s] - константа [целое|с плавающей точкой|строка]
    //v - переменная
    //o - операция
    //f - функция
    //l - ссылка
    if (QRegExp("("+Evaluation::constIntPattern()+")").exactMatch(aLexem))
    {
        return QString("ci:%1").arg(aLexem);
    }
    else if (QRegExp("("+Evaluation::constFloatPattern()+")").exactMatch(aLexem))
    {
        return QString("cf:%1").arg(aLexem);
    }
    else if (QRegExp("("+Evaluation::constStringPattern()+")").exactMatch(aLexem))
    {                
        QString string = aLexem;
        string.chop(1);
        string.remove(0,1);
        int ind = constStrings.size();
        constStrings.append(string);
        return QString("cs:%1").arg(ind);
    }
    else if (QRegExp(Evaluation::varPattern()).exactMatch(aLexem) &&
             aLexem.contains(QRegExp("[A-Za-z_]+")))
    {
        usedVars << aLexem;        
        return QString("v:%1").arg(aLexem);
    }
    else
    {        
        return aLexem;
    }
}

QString Evaluation::operationAnd(const QStringList & aParameters)
{    
    if (aParameters.size() != 2)
    {
        return "";
    }
    QList<int> possible;
    possible << -1 << 0 << 1 << 2;
    int first = aParameters[0].toInt();
    int second = aParameters[1].toInt();
    int result;
    if (possible.contains(first) && possible.contains(second))
    {
        int logicAnd[4][4] = {{-1,0,0,0},{0,0,0,0},{0,0,1,2},{0,0,2,2}};
        result = logicAnd[first+1][second+1];
    }
    else
    {
        result = first & second;
    }

    return QString("%1").arg(result);
}

QString Evaluation::operationOr(const QStringList & aParameters)
{    
    if (aParameters.size() != 2)
    {
        return "";
    }
    QList<int> possible;
    possible << -1 << 0 << 1 << 2;
    int first = aParameters[0].toInt();
    int second = aParameters[1].toInt();
    int result;
    if (possible.contains(first) && possible.contains(second))
    {
        int logicOr[4][4]  = {{-1,0,1,2},{0,0,1,2},{1,1,1,1},{2,2,1,2}};
        result = logicOr[first+1][second+1];
    }
    else
    {
        result = first | second;
    }

    return QString("%1").arg(result);
}

QString Evaluation::functionDep(const QStringList & aParameters)
{
    uint workable = 0,
         noData   = 0,
         disable  = 0;
    bool ok;
    int procent = aParameters[0].toInt(&ok,10);
    uint count  = aParameters.size()-1;

    if (!ok)
    {
        return QString("%1").arg(Global::NoData);
    }

    for (int i=1; i<aParameters.size(); i++)
    {
        switch (aParameters[i].toInt())
        {
            case Global::NoData:
                noData++;
                break;
            case Global::Disable:
                disable++;
                break;
            case Global::Workable:
                workable++;
                break;
        };
    }

    if (workable == count)
    {
        return QString("%1").arg(Global::Workable);
    }
    if (noData == count)
    {
        return QString("%1").arg(Global::NoData);
    }
    if ( (double)(noData+disable)/count*100 > (double)(100-procent))
    {
        return QString("%1").arg(Global::Disable);
    }
    else
    {
        return QString("%1").arg(Global::WorkableButBroken);
    }
}

QString Evaluation::functionEqual(const QStringList & aParameters)
{
    if (aParameters.size() != 2)
    {
        return "";
    }
    if (aParameters[0] == aParameters[1])
    {
        return "1";
    }
    else
    {
        return "0";
    }
}

QString Evaluation::functionNonEqual(const QStringList & aParameters)
{
    if (aParameters.size() != 2)
    {
        return "";
    }
    if (aParameters[0] == aParameters[1])
    {
        return "0";
    }
    else
    {
        return "1";
    }
}

QString Evaluation::functionMult(const QStringList & aParameters)
{
    //Строки при умножении считаются за 0
    double result = 1;
    for (int i=0; i<aParameters.size(); i++)
    {
        if (QRegExp(Evaluation::constIntPattern()).exactMatch(aParameters[i]))
        {
            result *= aParameters[i].toInt();
        }
        else if (QRegExp(Evaluation::constFloatPattern()).exactMatch(aParameters[i]))
        {
            result *= aParameters[i].toDouble();
        }
        else
        {
            result *= 0;
        }
    }
    return QString("%1").arg(result);
}

QString Evaluation::functionSum(const QStringList & aParameters)
{
    //Если есть хоть одна строка, то все остальные параметры тоже считаются строками
    double result = 0;
    bool hasString = false;
    for (int i=0; i<aParameters.size(); i++)
    {
        if (QRegExp(Evaluation::constIntPattern()).exactMatch(aParameters[i]))
        {
            result += aParameters[i].toInt();
        }
        else if (QRegExp(Evaluation::constFloatPattern()).exactMatch(aParameters[i]))
        {
            result += aParameters[i].toDouble();
        }
        else
        {
            hasString = true;
            break;
        }
    }
    if (hasString)
    {
        QString resultAsString;
        for (int i=0; i<aParameters.size(); i++)
        {
            resultAsString += aParameters[i];
        }
        return resultAsString;
    }
    else
    {
        return QString("%1").arg(result);
    }
}

QString Evaluation::functionSwitch(const QStringList & aParameters)
{
    //Должно быть четное количество параметров
    //Если выполняются несколько условий, то принимается первое
    //Условие выполняется при неравенстве выражения 0
    //Если выражение является строкой, то условие не выполняется
    //Если ни одно из условий не выполняется, то возвращается пустое значение
    if (aParameters.size()%2)
    {
        return "";
    }
    for (int i=0; i<aParameters.size(); i+=2)
    {
        if (QRegExp(Evaluation::constIntPattern()).exactMatch(aParameters[i]))
        {
            if (aParameters[i].toInt())
            {
                return aParameters[i+1];
            }
        }
        else if (QRegExp(Evaluation::constFloatPattern()).exactMatch(aParameters[i]))
        {
            if (aParameters[i].toDouble() != 0.0)
            {
                return aParameters[i+1];
            }
        }
    }
    return "";
}

QString Evaluation::functionMore(const QStringList & aParameters)
{
    if(aParameters.size()!=2)
    {
        return "0";
    }

    double num1=0;
    double num2=0;
    bool isNum=false;
    bool howString=false;


    num1 = toNum(aParameters[0],&isNum);
    if (!isNum)
    {
        howString=true;
    }

    num2 = toNum(aParameters[1],&isNum);
    if (!isNum)
    {
        howString=true;
    }

    if (howString)
    {
        if (aParameters[0] > aParameters[1])
        {
            return QString("1");
        }
        return QString("0");
    }
    else
    {
        if (num1>num2)
        {
            return "1";
        }
        else
        {
            return "0";
        }
    }
}

QString Evaluation::functionLess(const QStringList & aParameters)
{
    if(aParameters.size()!=2)
    {
        return "0";
    }
    QString result;

    result = functionMore(aParameters);
    if (result=="1" || aParameters[0]==aParameters[1])
    {
        return QString("0");
    }
    else
    {
        return QString("1");
    }
}

QString Evaluation::functionInRange(const QStringList & aParameters)
{
    if(aParameters.size()!=3)
    {
        return "0";
    }
    if ( (  (functionMore(QStringList()<<aParameters[0]<<aParameters[1])=="1") ||
            (aParameters[0] == aParameters[1])
          )
        &&
         (  (functionMore(QStringList()<<aParameters[2]<<aParameters[0])=="1") ||
            (aParameters[2] == aParameters[0])
          )
       )
    {
        return QString("1");
    }
    else
    {
        return QString("0");
    }
}

QString Evaluation::functionMid(const QStringList & aParameters)
{
    if(aParameters.size()!=3)
    {
        return "0";
    }

    quint16 num=0;
    int left=0;
    int size=0;
    bool isNum=false;
    bool howString=false;

    left = (int)toNum(aParameters[0],&isNum);
    if (!isNum)
    {
        howString=true;
    }

    size = (int)toNum(aParameters[1],&isNum);
    if (!isNum)
    {
        howString=true;
    }

    num = (int)toNum(aParameters[2],&isNum);
    if (!isNum)
    {
        howString=true;
    }

    if (howString)
    {
        return QString("0");
    }
    else
    {
        num <<= left;
        num >>= 16-size;
        return QString("%1").arg(num);
    }
}

// mid64(0,12,num)
// взять младших 12 бит num
QString Evaluation::functionMid64(const QStringList & aParameters)
{
    if(aParameters.size() != 3)
    {
        return "0";
    }

    quint64 num=0;
    int right=0;
    int size=0;
    bool isNum=false;
    bool howString=false;

    right = (int)toNum(aParameters[0],&isNum);
    if (!isNum)
    {
        howString=true;
    }

    size = (int)toNum(aParameters[1],&isNum);
    if (!isNum)
    {
        howString=true;
    }

    num = aParameters[2].toULongLong(&isNum);
    if (!isNum)
    {
        howString=true;
    }

    if (howString)
    {
        return QString("0");
    }
    else
    {
        num <<= (64-size-right);
        num >>= (64-size);
        return QString("%1").arg(num);
    }
}

QString Evaluation::functionStrF(const QStringList & aParameters)
{
    if(aParameters.size()!=2)
    {
        return "0";
    }

    int width=0;
    QString str=aParameters[1];
    bool isNum;

    width = (int)toNum(aParameters[0],&isNum);
    if (!isNum)
    {
        return QString("0");
    }
    QString result = str.left(width);
    if (result.size() && !result.at(result.size()-1).isDigit())
    {
        result.chop(1);
    }
    return result;
}

double Evaluation::toNum(QString str, bool *ok)
{
    double result = 0.0;
    bool equal=false;
    result = str.toInt(&equal,10);
    if (equal)
    {
        *ok=true;
        return result;
    }

    result = str.toInt(&equal,16);
    if (equal)
    {
        *ok=true;
        return result;
    }        

    result = str.toDouble(&equal);
    if (equal)
    {
        *ok=true;
        return result;
    }
    *ok=false;
    return 0.0;
}

QString Evaluation::functionMessage(const QStringList & aParameters)
{
    if (aParameters.size() == 2)
    {
        emit signalMessage(aParameters[0],aParameters[1]);
    }
    return QString("1");
}

QString Evaluation::functionMessageIf(const QStringList & aParameters)
{
    if (aParameters.size() == 3)
    {
        if (aParameters[0] != "0")
        {
            emit signalMessage(aParameters[1],aParameters[2]);
        }
    }
    return QString("1");
}

QString Evaluation::functionMod(const QStringList & aParameters)
{
    bool ok = false;
    bool ok1 = false;
    int num = aParameters[0].toInt(&ok,10);
    int num1 = aParameters[1].toInt(&ok1,10);
    if (ok && ok1)
    {
        return QString("%1").arg(num % num1);
    }
    return QString("");
}

QString Evaluation::functionNot(const QStringList & aParameters)
{
    bool ok = false;
    int num = aParameters[0].toInt(&ok,10);
    if (ok)
    {
        return QString("%1").arg(!num);
    }
    return QString("");
}

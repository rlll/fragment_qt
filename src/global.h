#ifndef GLOBAL_H
#define GLOBAL_H

#include <QStringList>


struct sizes
{
    double defaultW, defaultH; // размеры шалона
    double realW, realH;       // размеры виджета
    double resultW, resultH;   // размеры шаблона в виджите
};

class Global{
public:
    //Возможные состояния свойств
    enum PropertyStatus {NoData = -1,            //нет данных
                         Disable = 0,            //неработоспособен
                         Workable = 1,           //работоспособен и исправен
                         WorkableButBroken = 2   //работоспособен но неисправен
                        };
    enum {NormalTimeout = 3000};   //милисекунды
    enum {FullSleepTime = 800000}; //микросекунды
};

class UserFunction
{
public:
    UserFunction(){}

    QString name;
    QStringList arguments;
    QString eval;
    QList <QStringList> conditionLexems;
    QStringList usedVars;
    QStringList cS;
};
#endif

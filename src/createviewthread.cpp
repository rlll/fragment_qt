#include "createviewthread.h"

CreateViewThread::CreateViewThread(QObject *parent) :
    QObject(parent)
{
}

void CreateViewThread::slotCreatePicture(QString aContent, QString mime,
                                         QString aTemplate,
                                         int aWidth, int aHeight,
                                         QHash <QString,QString> aHrefById, QStringList aIds,
                                         bool aNeedResized, QObject* widget)
{
    QSvgRenderer renderer(aContent.toUtf8());
    sizes s = getSizes(renderer.viewBox().width(),
                       renderer.viewBox().height(),
                       aWidth,
                       aHeight);

    bool resized = false;

    QImage image(QSize((int)s.resultW,(int)s.resultH),QImage::Format_ARGB32);
    if ((int)s.resultW < 0 || (int)s.resultH < 0)
    {
        return ;
    }
    image.fill(0);
    QPainter painter(&image);
    renderer.render(&painter);

    QHash<QString, QRectF> hash;
    QHash<QString, QRectF> hashIds;

    if (aNeedResized)
    {
        QRectF rectF;
        QMatrix matrix;
        QHashIterator <QString,QString> itr(aHrefById);
        while (itr.hasNext())
        {
            itr.next();
            if (renderer.elementExists(itr.key()))
            {
                rectF = renderer.boundsOnElement(itr.key());
                matrix = renderer.matrixForElement(itr.key());
                rectF.moveTo(rectF.x()+matrix.dx(),
                             rectF.y()+matrix.dy());
                hash.insert(itr.key(),rectF);
            }
        }
        resized = true;

        for (int i=0; i<aIds.size(); i++)
        {
            if (renderer.elementExists(aIds.at(i)))
            {
                rectF = renderer.boundsOnElement(aIds.at(i));
                matrix = renderer.matrixForElement(aIds.at(i));
                rectF.moveTo(rectF.x()+matrix.dx(),
                             rectF.y()+matrix.dy());
                hashIds.insert(QString(aIds.at(i)).remove("_bac"),rectF);
            }
        }
    }

    emit readyPicture(image,resized,s,hash, hashIds, widget);
}

sizes CreateViewThread::getSizes(int aRendererWidth, int aRendererHeight,
               int aWdgWidth, int aWdgHeight)
{
    sizes result;

    result.defaultW = aRendererWidth;
    result.defaultH = aRendererHeight;
    result.realW = aWdgWidth;
    result.realH = aWdgHeight;


    double h = result.defaultH*result.realW/result.defaultW;
    if (h >= result.realH) //расчет по высоте
    {
        result.resultH = result.realH*0.95;
        result.resultW = result.defaultW*result.realH/result.defaultH*0.95;
    }
    else                   //расчет по ширине
    {
        result.resultW = result.realW*0.95;
        result.resultH = h*0.95;
    }
    return result;
}

void CreateViewThread::slotActivatedTemplates(QStringList aTemplates)
{
    activeTemplates = aTemplates;
}

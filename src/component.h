#ifndef COMPONENT_H
#define COMPONENT_H

#include <QMutex>
#include <QObject>
#include <QVariant>

#include "application.h"
#include "configcomponent.h"
#include "createviewthread.h"
#include "evaluation.h"
#include "eventlog.h"
#include "transferqueue.h"

class Component : public QObject
{
    Q_OBJECT
public :        
    Component(ConfigComponent,QObject *parent = 0);

    ~Component();

    int count;
    bool init();
    QString lastError();

    //Имя компонента
    QString getName() {return name;}
    //Описание компонента
    void setDescription(const QString & d) {description = d;}
    QString getDescription() {return description;}

    void setProperty(QString property, QString value);
    void setProperties(QStringList properties, QStringList values);

    bool makeData();
    void null();
    //Возвращает значение свойства по имени
    QString getValue(const QString &);

    bool hasTemplate(const QString &);
    void createViewByTemplate(const QString &);
    void deleteBuffer(const QString &);
    void setActiveTemplates(QStringList);

    TransferQueue <QHash<QString,QString> > *changedData;
    TransferQueue <QList <EventProp> > *changedEvent;

protected:
    void releaseProperties();
    void evaluate(const QStringList &);
    QString createView(const QString &, QStringList aChanged = QStringList());
    void createViewFromCache(const QString &, const QString &, const QString &);
    void createViewsOnScreens(bool);
    QStringList hasTemplateProperties(const QString &, QHash<QString,QString>*);
    QStringList hasTemplatePropertiesFromCache(const QString &);

private:
    ConfigComponent config;            //Ссылка на конфигурацию компонента

    QString name;                       //Имя компонента
    QString description;                //Описание компонента

    QHash<QString,QString> propertiesV2;
    QHash<QString,bool> propertiesChanges;
    QHash<QString,Evaluation*> evalsV2;
    QMultiHash<QString,QString> propDepsV2;
    QMultiHash<QString,QString> templatesPropertiesV2;
    QHash<QString,QString> templatesCacheV2;

    QHash<QString,QMap<int,QString> > templatesIndexes;
    QHash<QString,QHash<QString,QPair<int,int> > > templatesDiffs;
    QHash<QString,QString> viewsCache;

    QStringList activeTemplates;

    QStringList templates;              //Шаблоны компонента
    QStringList viewsTypes;             //Типы файлов отображений

    QList<Evaluation*> *commandsConditions; //Список объектов вычислений условий выполнения команд

    QString errorText;
    int counter;
    bool hasChanged;

    QString createSourceNormalId(QString source);   
    void setNormalId(QDomNode *);

    void createHashTextValueToGId(QString source);
    void setTextValueByGId(QDomNode *);
    QHash <QString,QString> hashTextValueToGId;

signals:
    void viewCreated(const QString &, const QString &, const QString &, const QString &, bool);
    void viewChanged(const QString &, const QString &, const QString &, const QString &, bool, int);

    void dataNulled(const QString &);
    void dataChanged();
    void eventChanged();
    void logMessage(const QString &);
    void command(const QString &,
                 const QString &,
                 const QString &,
                 const QList<QPair <QString,QString> > &,
                 const QList<QPair <QString,QString> > &,
                 int);
    void needSound(const QString &);

    void signalMessage(const QString &,const QString &);
    void changedButtonText(QString, QHash<QString,QString>);

    void storeData(QHash<QString,QString>);
public slots:
    void slotUpdateView();
};

#endif

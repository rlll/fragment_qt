#include "configcomponent.h"
#include "evaluation.h"

//PropertyDescription
PropertyDescription::PropertyDescription()
{
    display = "value";
    isTrigger = false;
}

void PropertyDescription::copyWithPrefix(const QString & aPrefix,
                                         const PropertyDescription & aProperty)
{
    isTrigger = aProperty.isTrigger;
    alias = aProperty.alias;
    QString displayRule = aProperty.display;
    display = (displayRule.isEmpty())?(displayRule):(aPrefix+displayRule);
    QString eventRule = aProperty.event;
    event = (eventRule.isEmpty())?(eventRule):(aPrefix+eventRule);
    QString databaseRule = aProperty.database;
    database = (databaseRule.isEmpty())?(databaseRule):(aPrefix+databaseRule);
    QString soundRule = aProperty.sound;
    sound = (soundRule.isEmpty())?(soundRule):(aPrefix+soundRule);
    QString groupName = aProperty.group;
    group = (groupName.isEmpty())?(groupName):(aPrefix+groupName);
    eval = aProperty.eval;    

    conditonValue = aProperty.conditonValue;
    conditonLexems = aProperty.conditonLexems;

    valueUsedVars = aProperty.valueUsedVars;
    lexemsUsedVars = aProperty.lexemsUsedVars;

    valueCS = aProperty.valueCS;
    lexemsCS = aProperty.lexemsCS;
}

//TemplateDescription
TemplateDescription::TemplateDescription()
{
    extension = "svg";
    isPrimary = false;
    isBottom = false;
}

//EventDescription
EventDescription::EventDescription()
{
}

//ConfigComponent
ConfigComponent::ConfigComponent() : Configuration()
{
    type = "component";

    DefaultValue = "-1";
}

void ConfigComponent::clearData()
{
    name = "";
    description = "";

    includes.clear();

    displayReplaceRules.clear();

    eventRules.clear();
    eventTypeRules.clear();
    alterEventRule.clear();
    alterEventTypeRule.clear();

    databaseReplaceRules.clear();

    soundRules.clear();

    propertiesV2.clear();

    templatesV2.clear();

    commandsConditions.clear();
    commandsAbonents.clear();
    commandsProtocols.clear();
    commandsMessages.clear();
    commandsFrom.clear();
    commandsTo.clear();
    commandsDelays.clear();
    commandLexems.clear();
    commandUsedVars.clear();
    commandCS.clear();

    targets.clear();
    messages.clear();
    protocols.clear();
    times.clear();
    targetsFrom.clear();
    targetsTo.clear();

    outsideProtocols.clear();
    outsideMessages.clear();

    dbGroups.clear();
}

bool ConfigComponent::parseDocument(QDomElement aRoot)
{
    QDomNode childNode;
    QDomNode child2Node;
    QDomNode child3Node;

    QDomNode firstNode = aRoot.firstChild();
    while (!firstNode.isNull())
    {
        if (firstNode.toElement().tagName() == "include") //include
        {
            if (firstNode.toElement().hasAttribute("component"))
            {
                includes.append(firstNode.toElement().attributeNode("component").value().trimmed());
            }
        }
        firstNode = firstNode.nextSibling();
    }
    QStringList parts = xmlFile.split("/");
    parts.removeLast();
    QString path = parts.join("/");
    for (int i=0; i<includes.size(); i++)
    {
        ConfigComponent include;
        if (include.parse(path+"/"+includes[i]+".xml"))
        {
            //rules
            if (include.displayReplaceRules.size() > 0)
            {
                QMapIterator<QString,ReplaceRule> displayRuleItr(include.displayReplaceRules);
                while (displayRuleItr.hasNext())
                {
                    displayRuleItr.next();
                    displayReplaceRules[include.name+":"+displayRuleItr.key()] = displayRuleItr.value();
                }
            }
            if (include.eventRules.size() > 0)
            {
                QMapIterator<QString,QMap<QString,QString> > eventRulesItr(include.eventRules);
                while (eventRulesItr.hasNext())
                {
                    eventRulesItr.next();
                    eventRules[include.name+":"+eventRulesItr.key()] = eventRulesItr.value();
                }
            }
            if (include.eventTypeRules.size() > 0)
            {
                QMapIterator<QString,QMap<QString,QString> > eventTypeRulesItr(include.eventTypeRules);
                while (eventTypeRulesItr.hasNext())
                {
                    eventTypeRulesItr.next();
                    eventTypeRules[include.name+":"+eventTypeRulesItr.key()] = eventTypeRulesItr.value();
                }
            }
            if (include.alterEventRule.size())
            {
                QMapIterator<QString,QString> alterEventRuleItr(include.alterEventRule);
                while (alterEventRuleItr.hasNext())
                {
                    alterEventRuleItr.next();
                    alterEventRule[include.name+":"+alterEventRuleItr.key()] = alterEventRuleItr.value();
                }
            }
            if (include.alterEventTypeRule.size())
            {
                QMapIterator<QString,QString> alterEventTypeRuleItr(include.alterEventTypeRule);
                while (alterEventTypeRuleItr.hasNext())
                {
                    alterEventTypeRuleItr.next();
                    alterEventTypeRule[include.name+":"+alterEventTypeRuleItr.key()] =alterEventTypeRuleItr.value();
                }
            }
            if (include.databaseReplaceRules.size() > 0)
            {
                QMapIterator<QString,ReplaceRule> databaseRuleItr(include.databaseReplaceRules);
                while (databaseRuleItr.hasNext())
                {
                    databaseRuleItr.next();
                    databaseReplaceRules[include.name+":"+databaseRuleItr.key()] = databaseRuleItr.value();
                }
            }
            if (include.soundRules.size() > 0)
            {
                QMapIterator<QString,QMap<QString,QString> > soundRulesItr(include.soundRules);
                while (soundRulesItr.hasNext())
                {
                    soundRulesItr.next();
                    soundRules[include.name+":"+soundRulesItr.key()] = soundRulesItr.value();
                }
            }
            //dbGroups
            if (include.dbGroups.size() > 0)
            {
                QMapIterator<QString,QString> dbGroupsItr(include.dbGroups);
                while (dbGroupsItr.hasNext())
                {
                    dbGroupsItr.next();
                    dbGroups[include.name+":"+dbGroupsItr.key()] = dbGroupsItr.value();
                }
            }
            //properties
            QMapIterator<QString,PropertyDescription> propertyItr(include.propertiesV2);
            while (propertyItr.hasNext())
            {
                propertyItr.next();
                propertiesV2[propertyItr.key()] = PropertyDescription();
                propertiesV2[propertyItr.key()].copyWithPrefix(include.name+":",propertyItr.value());
            }
            //templates
            QMapIterator<QString,TemplateDescription> templateItr(include.templatesV2);
            while (templateItr.hasNext())
            {
                templateItr.next();
                templatesV2[templateItr.key()] = templateItr.value();
                templatesV2[templateItr.key()].isPrimary = false;
            }
            //commands
            for (int j=0; j<include.commandsConditions.size(); j++)
            {
                commandsConditions.append(include.commandsConditions.value(j));
                commandsAbonents.append(include.commandsAbonents.value(j));
                commandsProtocols.append(include.commandsProtocols.value(j));
                commandsMessages.append(include.commandsMessages.value(j));
                commandsFrom.append(include.commandsFrom.value(j));
                commandsTo.append(include.commandsTo.value(j));

                commandsDelays.append(include.commandsDelays.value(j));
                commandLexems.append(include.commandLexems.value(j));
                commandUsedVars.append(include.commandUsedVars.value(j));
                commandCS.append(include.commandCS.value(j));
            }
            //targets
            for (int j=0; j<include.targets.size(); j++)
            {
                QString target = include.targets.value(j);
                targets.append(target);
                protocols[target] = include.protocols.value(target);
                messages[target] = include.messages.value(target);
                targetsFrom[target] = include.targetsFrom.value(target);
                targetsTo[target] = include.targetsTo.value(target);
                times[target] = include.times.value(target);
            }
            //outside
            for (int j=0; j<include.outsideProtocols.size(); j++)
            {
                outsideProtocols.append(include.outsideProtocols.value(j));
                outsideMessages.append(include.outsideMessages.value(j));
            }
        }
        else
        {
            errorText = QObject::tr("Ошибка в подключаемом компоненте '")+includes[i]+"':\n"+
                        include.lastError();
            return false;
        }
    }

    QDomNode node = aRoot.firstChild();
    while (!node.isNull())
    {
        if (node.toElement().tagName() == "name") //name
        {
            name = node.firstChild().toText().data().trimmed();
        }
        else if (node.toElement().tagName() == "description") //description
        {
            description = node.firstChild().toText().data().trimmed();
        }
        else if (node.toElement().tagName() == "groups")
        {
            childNode = node.firstChild();
            while (!childNode.isNull())
            {
                if (childNode.toElement().tagName() == "group")
                {
                    if (childNode.toElement().hasAttribute("name"))
                    {
                        QString name = childNode.toElement().attributeNode("name").value().trimmed();
                        if (!QRegExp("[a-zA-Z0-9_]+").exactMatch(name))
                        {
                            errorText = QObject::tr("Указано некорректное имя группы '")+name+"'.";
                            return false;
                        }
                        QString alias;
                        if (childNode.toElement().hasAttribute("alias"))
                        {
                            alias = childNode.toElement().attributeNode("alias").value().trimmed();
                        }
                        else
                        {
                            alias = name;
                        }
                        dbGroups[name] = alias;
                    }
                    else
                    {
                        errorText = QObject::tr("Не указано имя группы.");
                        return false;
                    }
                }
                childNode = childNode.nextSibling();
            }
        }

        // Пользовательская функция
        else if (node.toElement().tagName() == "user-function")
        {
            childNode = node.firstChild();
            while (!childNode.isNull())
            {
                if (childNode.toElement().tagName() == "function")
                {
                    UserFunction userF;
                    QString fName;
                    if (childNode.toElement().hasAttribute("name"))
                    {
                        QString fName = childNode.toElement().attributeNode("name").value().trimmed();
                        userF.name = fName;
                        if (!QRegExp("[a-zA-Z0-9_]+").exactMatch(fName))
                        {
                            errorText = QObject::tr("Указано некорректное название пользвательской функции '")+name+"'.";
                            return false;
                        }
                        if (nameUserFunctions.contains(fName))
                        {
                            errorText = QObject::tr("Повторяющееся название пользвательской функции '")+name+"'.";
                            return false;
                        }
                        nameUserFunctions.append(fName);
                    }
                    else
                    {
                        errorText = QObject::tr("Не указано название функции.");
                        return false;
                    }

                    QStringList arguments;
                    if (childNode.toElement().hasAttribute("arguments"))
                    {
                        arguments = childNode.toElement().attributeNode("arguments").value()
                                    .trimmed().split(",",QString::SkipEmptyParts);
                        for (int i=0; i<arguments.size(); i++)
                        {
                            if (!QRegExp("[a-zA-Z0-9_]+").exactMatch(arguments[i]))
                            {
                                errorText = QObject::tr("Указано некорректное название аргумета:'")+arguments[i]
                                            +"' пользовательской функциии:"+ fName;
                                return false;
                            }
                        }
                    }
                    userF.arguments = arguments;

                    QString eval;
                    if (childNode.toElement().hasAttribute("eval"))
                    {
                        eval = childNode.toElement().attributeNode("eval").value().trimmed();
                        if (eval.isEmpty())
                        {
                            errorText = QObject::tr("Отуствует описание функции:'")+fName;
                            return false;
                        }
                        Evaluation ev(eval,nameUserFunctions);
                        errorText = ev.errorText;
                        if (!errorText.isEmpty())
                        {
                            errorText = QString("\nОшибка в function: %1\n\t%2").arg(fName)
                                        .arg(errorText);
                            return false;
                        }
                        userF.eval = eval;
                        userF.conditionLexems = ev.getConditionLexems();
                        userF.usedVars = ev.getUsedVars();
                        userF.cS = ev.getCS();
                    }
                    else
                    {
                        errorText = QObject::tr("Не указано описание функции.");
                        return false;
                    }
                    userFunctions[userF.name] = userF;
                }

                childNode = childNode.nextSibling();
            }
        }
        else if (node.toElement().tagName() == "display-rules")
        {
            childNode = node.firstChild();
            while (!childNode.isNull())
            {
                if (childNode.toElement().tagName() == "rule")
                {
                    if (childNode.toElement().hasAttribute("name"))
                    {
                        QString ruleName = childNode.toElement().attributeNode("name").value().trimmed();
                        if (!QRegExp("[a-zA-Z0-9_]+").exactMatch(ruleName))
                        {
                            errorText = QObject::tr("Указано некорректное имя правила отображения '")+
                                        ruleName+"'.";
                            return false;
                        }
                        displayReplaceRules[ruleName] = ReplaceRule();
                        child2Node = childNode.firstChild();
                        while (!child2Node.isNull())
                        {
                            if (child2Node.toElement().tagName() == "replace")
                            {
                                if (child2Node.toElement().hasAttribute("before") &&
                                    child2Node.toElement().hasAttribute("after"))
                                {
                                    QString before = child2Node.toElement().attributeNode("before").value();
                                    QString after = child2Node.toElement().attributeNode("after").value();
                                    displayReplaceRules[ruleName].addReplace(before,after);
                                }
                                else if (child2Node.toElement().hasAttribute("before-reg") &&
                                         child2Node.toElement().hasAttribute("after"))
                                {
                                    QString beforeReg = child2Node.toElement().attributeNode("before-reg").value();
                                    QString after = child2Node.toElement().attributeNode("after").value();
                                    displayReplaceRules[ruleName].addRegReplace(beforeReg,after);
                                }
                                else if (child2Node.toElement().hasAttribute("before-eval") &&
                                         child2Node.toElement().hasAttribute("after"))
                                {
                                    QString beforeEval = child2Node.toElement().attributeNode("before-eval").value();
                                    QString after = child2Node.toElement().attributeNode("after").value();                                    

                                    Evaluation ev(beforeEval,nameUserFunctions);
                                    errorText = ev.errorText;
                                    if (!errorText.isEmpty())
                                    {
                                        errorText = QString("\nОшибка в display-rules: %1\n\t%2").arg(ruleName)
                                                    .arg(errorText);
                                        return false;
                                    }

                                    displayReplaceRules[ruleName].addEvalReplace(ev.getConditionLexems(),
                                                                                  ev.getUsedVars(),
                                                                                  ev.getCS(),
                                                                                  after);
                                }
                                else if (child2Node.toElement().hasAttribute("after"))
                                {
                                    if (!displayReplaceRules[ruleName].hasAlternativeReplace())
                                    {
                                        QString after = child2Node.toElement().attributeNode("after").value();
                                        displayReplaceRules[ruleName].addAlternativeReplace(after);
                                    }
                                    else
                                    {
                                        errorText = QObject::tr("Дублирование альтернативы в правиле отображения '")+
                                                    ruleName+"'.";
                                        return false;
                                    }
                                }
                                else
                                {
                                    errorText = QObject::tr("Указана некорректная замена в правиле отображения '")+
                                                ruleName+"'.";
                                    return false;
                                }
                            }
                            child2Node = child2Node.nextSibling();
                        }
                    }
                    else
                    {
                        errorText = QObject::tr("Не указано имя правила отображения.");
                        return false;
                    }
                }
                childNode = childNode.nextSibling();
            }
        }
        else if (node.toElement().tagName() == "event-rules")
        {
            childNode = node.firstChild();
            while (!childNode.isNull())
            {
                if (childNode.toElement().tagName() == "rule")
                {
                    if (childNode.toElement().hasAttribute("name"))
                    {
                        QString ruleName = childNode.toElement().attributeNode("name").value().trimmed();
                        if (!QRegExp("[a-zA-Z0-9_]+").exactMatch(ruleName))
                        {
                            errorText = QObject::tr("Указано некорректное имя правила события '")+
                                        ruleName+"'.";
                            return false;
                        }

                        QString aliasName;
                        if (childNode.toElement().hasAttribute("alias"))
                        {
                            aliasName = childNode.toElement().attributeNode("alias").value().trimmed();
                        }
                        else
                        {
                            allWarningTexts.append(QObject::tr("Не указан alias события '")+
                                        ruleName+"'.");
                            aliasName = ruleName;
                        }

                        QMap<QString,QString> rules;
                        QMap<QString,QString> ruleTypes;
                        child2Node = childNode.firstChild();
                        while (!child2Node.isNull())
                        {
                            if (child2Node.toElement().tagName() == "replace")
                            {
                                if (child2Node.toElement().hasAttribute("after"))
                                {
                                    QString after = child2Node.toElement().attributeNode("after").
                                                                           value().trimmed();
                                    if (child2Node.toElement().hasAttribute("before"))
                                    {
                                        QString before = child2Node.toElement().attributeNode("before").
                                                                                value().trimmed();
                                        rules[before] = after;
                                        if (child2Node.toElement().hasAttribute("color"))
                                        {
                                            QString type = child2Node.toElement().
                                                                           attributeNode("color").
                                                                           value().trimmed().toLower();

                                            if (!QColor(type).isValid())/*EventLog::getTypeByString(type) == 0)*/
                                            {
                                                errorText = QObject::tr("Указан некорректный цвет '")+
                                                            type+QObject::tr("' в правиле события '")+
                                                            ruleName+"'.";
                                                return false;
                                            }
                                            ruleTypes[before] = type;
                                        }
                                        else
                                        {
                                            ruleTypes[before] = "usual";
                                        }
                                    }
                                    else
                                    {
                                        alterEventRule[ruleName] = after;
                                        if (child2Node.toElement().hasAttribute("color"))
                                        {
                                            QString type = child2Node.toElement().
                                                           attributeNode("color").
                                                           value().trimmed().toLower();
                                            if (!QColor(type).isValid())/*EventLog::getTypeByString(type) == 0)*/
                                            {
                                                errorText = QObject::tr("Указан некорректный тип события '")+
                                                            type+QObject::tr("' в правиле события '")+
                                                            ruleName+"'.";
                                                return false;
                                            }
                                            alterEventTypeRule[ruleName] = type;
                                        }
                                        else
                                        {
                                            alterEventTypeRule[ruleName] = "usual";
                                        }
                                    }
                                }
                                else
                                {
                                    errorText = QObject::tr("Не указан заменитель в правиле события '")+
                                                ruleName+"'.";
                                    return false;
                                }
                            }
                            child2Node = child2Node.nextSibling();
                        }
                        eventRulesAlias[ruleName] = aliasName;
                        eventRules[ruleName] = rules;
                        eventTypeRules[ruleName] = ruleTypes;
                    }
                    else
                    {
                        errorText = QObject::tr("Не указано имя правила события.");
                        return false;
                    }
                }
                childNode = childNode.nextSibling();
            }
        }
        else if (node.toElement().tagName() == "db-rules")
        {
            childNode = node.firstChild();
            while (!childNode.isNull())
            {
                if (childNode.toElement().tagName() == "rule")
                {
                    if (childNode.toElement().hasAttribute("name"))
                    {
                        QString ruleName = childNode.toElement().attributeNode("name").value().trimmed();
                        if (!QRegExp("[a-zA-Z0-9_]+").exactMatch(ruleName))
                        {
                            errorText = QObject::tr("Указано некорректное имя правила базы данных '")+
                                        ruleName+"'.";
                            return false;
                        }
                        databaseReplaceRules[ruleName] = ReplaceRule();
                        child2Node = childNode.firstChild();
                        while (!child2Node.isNull())
                        {
                            if (child2Node.toElement().tagName() == "replace")
                            {
                                if (child2Node.toElement().hasAttribute("before") &&
                                    child2Node.toElement().hasAttribute("after"))
                                {
                                    QString before = child2Node.toElement().attributeNode("before").value();
                                    QString after = child2Node.toElement().attributeNode("after").value();
                                    databaseReplaceRules[ruleName].addReplace(before,after);
                                }
                                else if (child2Node.toElement().hasAttribute("before-reg") &&
                                         child2Node.toElement().hasAttribute("after"))
                                {
                                    QString beforeReg = child2Node.toElement().attributeNode("before-reg").value();
                                    QString after = child2Node.toElement().attributeNode("after").value();
                                    databaseReplaceRules[ruleName].addRegReplace(beforeReg,after);
                                }
                                else if (child2Node.toElement().hasAttribute("before-eval") &&
                                         child2Node.toElement().hasAttribute("after"))
                                {
                                    QString beforeEval = child2Node.toElement().attributeNode("before-eval").value();
                                    QString after = child2Node.toElement().attributeNode("after").value();                                    

                                    Evaluation ev(beforeEval,nameUserFunctions);
                                    errorText = ev.errorText;
                                    if (!errorText.isEmpty())
                                    {
                                        errorText = QString("\nОшибка в db-rules: %1\n\t%2").arg(ruleName)
                                                    .arg(errorText);
                                        return false;
                                    }

                                    databaseReplaceRules[ruleName].addEvalReplace(ev.getConditionLexems(),
                                                                                  ev.getUsedVars(),
                                                                                  ev.getCS(),
                                                                                  after);

                                }
                                else if (child2Node.toElement().hasAttribute("after"))
                                {
                                    if (!displayReplaceRules[ruleName].hasAlternativeReplace())
                                    {
                                        QString after = child2Node.toElement().attributeNode("after").value();
                                        databaseReplaceRules[ruleName].addAlternativeReplace(after);
                                    }
                                    else
                                    {
                                        errorText = QObject::tr("Дублирование альтернативы в правиле базы данных '")+
                                                    ruleName+"'.";
                                        return false;
                                    }
                                }
                                else
                                {
                                    errorText = QObject::tr("Указана некорректная замена в правиле базы данных '")+
                                                ruleName+"'.";
                                    return false;
                                }
                            }
                            child2Node = child2Node.nextSibling();
                        }
                    }
                    else
                    {
                        errorText = QObject::tr("Не указано имя правила базы данных.");
                        return false;
                    }
                }
                childNode = childNode.nextSibling();
            }
        }
        else if (node.toElement().tagName() == "sound-rules")
        {
            childNode = node.firstChild();
            while (!childNode.isNull())
            {
                if (childNode.toElement().tagName() == "rule")
                {
                    if (childNode.toElement().hasAttribute("name"))
                    {
                        QString ruleName = childNode.toElement().attributeNode("name").value().trimmed();
                        if (!QRegExp("[a-zA-Z0-9_]+").exactMatch(ruleName))
                        {
                            errorText = QObject::tr("Указано некорректное имя правила звуковоспроизведения '")+
                                        ruleName+"'.";
                            return false;
                        }
                        QMap<QString,QString> rules;
                        child2Node = childNode.firstChild();
                        while (!child2Node.isNull())
                        {
                            if (child2Node.toElement().tagName() == "replace")
                            {
                                if (child2Node.toElement().hasAttribute("after"))
                                {
                                    QString after = child2Node.toElement().attributeNode("after").value().trimmed();
                                    if (child2Node.toElement().hasAttribute("before"))
                                    {
                                        rules[child2Node.toElement().attributeNode("before").value().trimmed()] = after;
                                    }
                                    else
                                    {
                                        errorText = QObject::tr("Не указан исходник в правиле звуковоспроизведения '")+
                                                    ruleName+"'.";
                                        return false;
                                    }
                                }
                                else
                                {
                                    errorText = QObject::tr("Не указан заменитель в правиле звуковоспроизведения '")+
                                                ruleName+"'.";
                                    return false;
                                }
                            }
                            child2Node = child2Node.nextSibling();
                        }
                        soundRules[ruleName] = rules;
                    }
                    else
                    {
                        errorText = QObject::tr("Не указано имя правила звуковоспроизведения.");
                        return false;
                    }
                }
                childNode = childNode.nextSibling();
            }
        }
        else if (node.toElement().tagName() == "properties")  //properties or triggers
        {
            childNode = node.firstChild();
            while (!childNode.isNull())
            {
                if ((childNode.toElement().tagName() == "property") || //property or trigger
                    (childNode.toElement().tagName() == "trigger"))
                {                    
                    if (childNode.toElement().hasAttribute("name")) //property:name
                    {
                        QString propertyName = childNode.toElement().attributeNode("name").
                                               value().trimmed();
                        propertiesV2[propertyName] = PropertyDescription();
                        if (childNode.toElement().tagName() == "trigger")
                        {
                            propertiesV2[propertyName].isTrigger = true;
                        }

                        if (childNode.toElement().hasAttribute("alias")) //property:alias
                        {
                            propertiesV2[propertyName].alias = childNode.toElement().
                                                               attributeNode("alias").value().trimmed();
                        }
                        else
                        {
                            propertiesV2[propertyName].alias = propertyName;
                        }

                        if (childNode.toElement().hasAttribute("value")) //property:value
                        {
                            QString value = childNode.toElement().attributeNode("value").value().trimmed();
                            Evaluation ev(value,nameUserFunctions);
                            errorText = ev.errorText;
                            if (!errorText.isEmpty())
                            {
                                errorText = QString("\nОшибка в value: %1\n\t%2").arg(propertyName)
                                            .arg(errorText);
                                return false;
                            }
                            propertiesV2[propertyName].conditonValue = ev.getConditionLexems();
                            propertiesV2[propertyName].valueUsedVars = ev.getUsedVars();
                            propertiesV2[propertyName].valueCS = ev.getCS();
                        }
                        else if (childNode.toElement().tagName() == "trigger")
                        {
                            errorText = QObject::tr("Не указано значение триггера '")+propertyName+"'.";
                            return false;
                        }

                        if (childNode.toElement().hasAttribute("display-rule")) //property:display
                        {
                            QString display = childNode.toElement().attributeNode("display-rule").
                                                                              value().trimmed();
                            if (!QRegExp("[a-zA-Z0-9_]+").exactMatch(display))
                            {
                                errorText = QObject::tr("Указано некорректное имя правила отображения '")+
                                            display+QObject::tr("' в свойстве '")+propertyName+"'.";
                                return false;
                            }
                            propertiesV2[propertyName].display = display;
                        }
                        if (childNode.toElement().hasAttribute("event-rule")) //property:event
                        {
                            QString event = childNode.toElement().attributeNode("event-rule").
                                                                  value().trimmed();
                            if (!QRegExp("[\\w\\s*]+").exactMatch(event))
                            {
                                errorText = QObject::tr("Указано некорректное имя правила события '")+
                                            event+QObject::tr("' в свойстве '")+propertyName+"'.";
                                return false;
                            }
                            propertiesV2[propertyName].event = event;
                        }
                        if (childNode.toElement().hasAttribute("db-rule"))
                        {
                            QString db = childNode.toElement().attributeNode("db-rule").
                                                               value().trimmed();
                            if (!QRegExp("[a-zA-Z0-9_]+").exactMatch(db))
                            {
                                errorText = QObject::tr("Указано некорректное имя правила базы данных '")+
                                            db+QObject::tr("' в свойстве '")+propertyName+"'.";
                                return false;
                            }
                            propertiesV2[propertyName].database = db;
                        }
                        if (childNode.toElement().hasAttribute("sound-rule"))
                        {
                            QString sound = childNode.toElement().attributeNode("sound-rule").
                                                                  value().trimmed();
                            if (!QRegExp("[a-zA-Z0-9_]+").exactMatch(sound))
                            {
                                errorText = QObject::tr("Указано некорректное имя правила звуковоспроизведения '")+
                                            sound+QObject::tr("' в свойстве '")+propertyName+"'.";
                                return false;
                            }
                            propertiesV2[propertyName].sound = sound;
                        }

                        if (childNode.toElement().hasAttribute("group")) //property:group
                        {
                            QString group = childNode.toElement().attributeNode("group").
                                                                  value().trimmed();
                            if (!QRegExp("[a-zA-Z0-9_]+").exactMatch(group))
                            {
                                errorText = QObject::tr("Указано некорректное имя группы '")+group+
                                            QObject::tr("' в свойстве '")+propertyName+"'.";
                                return false;
                            }
                            propertiesV2[propertyName].group = group;
                        }

                        if (childNode.toElement().hasAttribute("eval")) //property:eval
                        {                            
                            propertiesV2[propertyName].eval = childNode.toElement().
                                                              attributeNode("eval").value().trimmed();                            
                            Evaluation ev(propertiesV2[propertyName].eval,nameUserFunctions);
                            errorText = ev.errorText;
                            if (!errorText.isEmpty())
                            {
                                errorText = QString("\nОшибка в property: %1\n\t%2").arg(propertyName)
                                            .arg(errorText);
                                return false;
                            }
                            propertiesV2[propertyName].conditonLexems = ev.getConditionLexems();
                            propertiesV2[propertyName].lexemsUsedVars = ev.getUsedVars();
                            propertiesV2[propertyName].lexemsCS = ev.getCS();
                        }
                    }
                    else
                    {
                        errorText = QObject::tr("Не указано имя свойства.");
                        return false;
                    }
                }
                childNode = childNode.nextSibling();
            }
        }

        else if (node.toElement().tagName() == "stores") //store
        {
            childNode = node.firstChild();
            while (!childNode.isNull())
            {
                if (childNode.toElement().tagName() == "store")
                {
                    QString property;
                    if (childNode.toElement().hasAttribute("property"))
                    {
                        property = childNode.toElement().attributeNode("property").value().trimmed();
                        if (property.isEmpty() || !propertiesV2.contains(property))
                        {
                            errorText = QObject::tr("Указано некорректное свойство наблюдаемого параметра '")+
                                        property+"'.";
                            return false;
                        }
                    }
                    stores.append(name+"@"+property);

                    QString interval;
                    if (childNode.toElement().hasAttribute("interval"))
                    {
                        interval = childNode.toElement().attributeNode("interval").value().trimmed();
                        bool isNum;
                        interval.toInt(&isNum);
                        if (!isNum)
                        {
                            errorText = QObject::tr("Указано некорректный интервал наблюдаемого параметра '")+
                                        property+"'.";
                            return false;
                        }
                    }
                    storeInterval.insert(name+"@"+property,interval.toInt());

                }
                childNode = childNode.nextSibling();
            }
        }
        else if (node.toElement().tagName() == "templates") //templates
        {
            childNode = node.firstChild();
            while (!childNode.isNull())
            {
                if (childNode.toElement().tagName() == "template") //template
                {
                    QString tmp = childNode.firstChild().toText().data();                    
                    templatesV2[tmp] = TemplateDescription();
                    if (childNode.toElement().hasAttribute("type"))
                    {
                        QString type = childNode.toElement().attributeNode("type").value().trimmed();
                        if (!QRegExp("(svg|html)").exactMatch(type.toLower()))
                        {
                            errorText = QObject::tr("Указан некорректный тип шаблона '")+type+"'.";
                            return false;
                        }
                        templatesV2[tmp].extension = type;
                    }
                    if (childNode.toElement().hasAttribute("primary"))
                    {
                        QString primary = childNode.toElement().attributeNode("primary").value().trimmed();
                        templatesV2[tmp].isPrimary = (primary.toLower() == "yes" || primary.toLower() == "y");
                    }
                    if (childNode.toElement().hasAttribute("bottom"))
                    {
                        QString bottom = childNode.toElement().attributeNode("bottom").value().trimmed();
                        templatesV2[tmp].isBottom = (bottom.toLower() == "yes" || bottom.toLower() == "y");
                    }
                    if (childNode.toElement().hasAttribute("name"))
                    {
                        QString name = childNode.toElement().attributeNode("name").value().trimmed();
                        templatesV2[tmp].name = name;
                    }                    
                }
                childNode = childNode.nextSibling();
            }
        }
        else if (node.toElement().tagName() == "commands") //commands
        {
            childNode = node.firstChild();
            while (!childNode.isNull())
            {
                if (childNode.toElement().tagName() == "command") //command
                {                    
                    if ((childNode.toElement().hasAttribute("condition")) &&
                        (childNode.toElement().hasAttribute("abonent")) &&
                        (childNode.toElement().hasAttribute("protocol")) &&
                        (childNode.toElement().hasAttribute("message")))
                    {
                        QString condition = childNode.toElement().attributeNode("condition").value().trimmed();
                        commandsConditions.append(condition);
                        Evaluation ev(condition,nameUserFunctions);
                        errorText = ev.errorText;
                        if (!errorText.isEmpty())
                        {
                            errorText = QString("\nОшибка в condition: %1\n\t%2").arg(condition)
                                        .arg(errorText);
                            return false;
                        }                        
                        commandLexems.append(ev.getConditionLexems());
                        commandUsedVars.append(ev.getUsedVars());
                        commandCS.append(ev.getCS());

                        QString abonent = childNode.toElement().attributeNode("abonent").value().trimmed();
                        if (!QRegExp("[a-zA-Z0-9_]+").exactMatch(abonent))
                        {
                            errorText = QObject::tr("В описании команды указано некорректное имя абонента '")+
                                        abonent+"'.";
                            return false;
                        }
                        QStringList comAbonents(abonent);                        
                        commandsAbonents.append(comAbonents);
                        QString protocol = childNode.toElement().attributeNode("protocol").value().trimmed();
                        if (!QRegExp("[a-zA-Z0-9_]+").exactMatch(protocol))
                        {
                            errorText = QObject::tr("В описании команды указано некорректное имя протокола '")+
                                        protocol+"'.";
                            return false;
                        }
                        QStringList comProtocols(protocol);
                        commandsProtocols.append(comProtocols);
                        QString mes = childNode.toElement().attributeNode("message").value().trimmed();
                        if (!QRegExp("[a-zA-Z0-9_]+").exactMatch(mes))
                        {
                            errorText = QObject::tr("В описании команды указан некорректный код сообщения '")+
                                        mes+"'.";
                            return false;
                        }
                        QStringList comMessages(mes);
                        commandsMessages.append(comMessages);

                        QList<QPair <QString,QString> > oneCommandTo;
                        if (childNode.toElement().hasAttribute("to"))
                        {
                            QStringList listTo = childNode.toElement().attributeNode("to").value().split(",");
                            for (int i=0;i<listTo.size();i++)
                            {
                                if (listTo.at(i).contains("'"))
                                {
                                    int count = listTo.at(i).count("'");
                                    if (count != 2)
                                    {
                                        errorText = QObject::tr("В описании команды to отсутсвует \"'\" условие команды = \"")+
                                                    condition+"\".";
                                        return false;
                                    }
                                }
                                QStringList onePair = listTo.at(i).split("=");
                                if (onePair.size() == 2)
                                {
                                    oneCommandTo.append(QPair <QString,QString>(onePair.at(0),onePair.at(1)));
                                }
                            }
                        }
                        QList <QList<QPair <QString,QString> > > fewCommandTo;
                        fewCommandTo.append(oneCommandTo);
                        commandsTo.append(fewCommandTo);

                        QList<QPair <QString,QString> > oneCommandFrom;
                        if (childNode.toElement().hasAttribute("from"))
                        {
                            QStringList listFrom = childNode.toElement().attributeNode("from").value().split(",");
                            for (int i=0;i<listFrom.size();i++)
                            {
                                if (listFrom .at(i).contains("'"))
                                {
                                    int count = listFrom.at(i).count("'");
                                    if (count != 2)
                                    {
                                        errorText = QObject::tr("В описании команды from отсутсвует \"'\" условие команды = \"")+
                                                    condition+"\".";
                                        return false;
                                    }
                                }
                                QStringList onePair = listFrom.at(i).split("=");
                                if (onePair.size() == 2)
                                {
                                    oneCommandFrom.append(QPair <QString,QString>(onePair.at(0),onePair.at(1)));
                                }
                            }
                        }
                        QList <QList<QPair <QString,QString> > > fewCommandFrom;
                        fewCommandFrom.append(oneCommandFrom);
                        commandsFrom.append(fewCommandFrom);

                        int delay = 0;
                        if (childNode.toElement().hasAttribute("delay"))
                        {
                            QString sDelay = childNode.toElement().attributeNode("delay").value().trimmed();
                            bool ok=true;
                            delay = sDelay.toInt(&ok,10);
                            if (!ok && delay >= 0)
                            {
                                errorText = QObject::tr("В описании команды указано некорректное время задржки '")+
                                            sDelay+"'.";
                                return false;
                            }
                        }

                        commandsDelays.append(QList<int>() << delay);
                    }
                    else
                    {
                        if (!childNode.toElement().hasAttribute("condition"))
                        {
                            errorText = QObject::tr("В описании команды не указано условие.");
                        }
                        else if (!childNode.toElement().hasAttribute("abonent"))
                        {
                            errorText = QObject::tr("В описании команды не указан абонент.");
                        }
                        else if (!childNode.toElement().hasAttribute("protocol"))
                        {
                            errorText = QObject::tr("В описании команды не указан протокол.");
                        }
                        else if (!childNode.toElement().hasAttribute("message"))
                        {
                            errorText = QObject::tr("В описании команды не указано сообщение.");
                        }
                        return false;
                    }
                }
                else if (childNode.toElement().tagName() == "multi-command") //multi-command
                {
                    if (childNode.toElement().hasAttribute("condition"))
                    {
                        QString condition = childNode.toElement().attributeNode("condition").value().trimmed();
                        QStringList comAbonents;
                        QStringList comProtocols;
                        QStringList comMessages;
                        QList<QList<QPair <QString,QString> > > comFrom;
                        QList<QList<QPair <QString,QString> > > comTo;
                        child2Node = childNode.firstChild();
                        while (!child2Node.isNull())
                        {
                            if (child2Node.toElement().tagName() == "command")
                            {
                                if ((child2Node.toElement().hasAttribute("abonent")) &&
                                    (child2Node.toElement().hasAttribute("protocol")) &&
                                    (child2Node.toElement().hasAttribute("message")))
                                {
                                    QString abonent = child2Node.toElement().attributeNode("abonent").
                                                                             value().trimmed();
                                    if (!QRegExp("[a-zA-Z0-9_]+").exactMatch(abonent))
                                    {
                                        errorText = QObject::tr("В описании множественной команды указано некорректное имя абонента '")+
                                        abonent+"'.";
                                        return false;
                                    }
                                    comAbonents << abonent;
                                    QString protocol = child2Node.toElement().attributeNode("protocol").
                                                                              value().trimmed();
                                    if (!QRegExp("[a-zA-Z0-9_]+").exactMatch(protocol))
                                    {
                                        errorText = QObject::tr("В описании множественной команды указано некорректное имя протокола '")+
                                                    protocol+"'.";
                                        return false;
                                    }
                                    comProtocols << protocol;
                                    QString mes = child2Node.toElement().attributeNode("message").
                                                                         value().trimmed();
                                    if (!QRegExp("[a-zA-Z0-9_]+").exactMatch(mes))
                                    {
                                        errorText = QObject::tr("В описании множественной команды указан некорректный код сообщения '")+
                                                    mes+"'.";
                                        return false;
                                    }
                                    comMessages << mes;

                                    QList<QPair <QString,QString> > oneCommandTo;
                                    if (child2Node.toElement().hasAttribute("to"))
                                    {
                                        QStringList listTo = child2Node.toElement().attributeNode("to").value().split(",");
                                        for (int i=0;i<listTo.size();i++)
                                        {
                                            QStringList onePair = listTo.at(i).split("=");
                                            if (onePair.size() == 2)
                                            {
                                                oneCommandTo.append(QPair<QString,QString> (onePair.at(0),onePair.at(1)));
                                            }
                                            else
                                            {
                                                oneCommandTo.append(QPair <QString,QString>());
                                            }
                                        }
                                    }
                                    comTo.append(oneCommandTo);

                                    QList<QPair <QString,QString> > oneCommandFrom;
                                    if (child2Node.toElement().hasAttribute("from"))
                                    {
                                        QStringList listFrom = child2Node.toElement().attributeNode("from").value().split(",");
                                        for (int i=0;i<listFrom.size();i++)
                                        {
                                            QStringList onePair = listFrom.at(i).split("=");                                            
                                            if (onePair.size() == 2)
                                            {
                                                oneCommandFrom.append(QPair<QString,QString> (onePair.at(0),onePair.at(1)));
                                            }
                                            else
                                            {
                                                oneCommandFrom.append(QPair <QString,QString>());
                                            }
                                        }
                                    }
                                    comFrom.append(oneCommandFrom);
                                }
                                else
                                {
                                    if (!child2Node.toElement().hasAttribute("abonent"))
                                    {
                                        errorText = QObject::tr("В описании команды не указан абонент.");
                                    }
                                    else if (!child2Node.toElement().hasAttribute("protocol"))
                                    {
                                        errorText = QObject::tr("В описании команды не указан протокол.");
                                    }
                                    else if (!child2Node.toElement().hasAttribute("message"))
                                    {
                                        errorText = QObject::tr("В описании команды не указано сообщение.");
                                    }
                                    return false;
                                }
                            }
                            child2Node = child2Node.nextSibling();
                        }
                        if ((comAbonents.size() > 0) && (comProtocols.size() > 0) && (comMessages.size() > 0))
                        {
                            commandsConditions.append(condition);
                            commandsAbonents.append(comAbonents);
                            commandsProtocols.append(comProtocols);
                            commandsMessages.append(comMessages);
                            commandsFrom.append(comFrom);
                            commandsTo.append(comTo);
                        }
                    }
                    else
                    {
                        errorText = QObject::tr("В описании множественной команды не указано условие.");
                        return false;
                    }
                }
                childNode = childNode.nextSibling();
            }
        }
        else if (node.toElement().tagName() == "targets") //targets
        {
            childNode = node.firstChild();
            while (!childNode.isNull())
            {
                if (childNode.toElement().tagName() == "target") //target
                {
                    if (childNode.toElement().hasAttribute("name")) //target:name
                    {
                        QString targetName = childNode.toElement().attributeNode("name").value().trimmed();
                        if (!QRegExp("[a-zA-Z0-9_]+").exactMatch(targetName))
                        {
                            errorText = QObject::tr("Указано некорректное имя абонента цели'")+
                            targetName+"'.";
                            return false;
                        }
                        targets.append(targetName);
                        QStringList messagesList;
                        QStringList protocolsList;
                        QList<quint64> timesList;
                        QList<QList<QPair<QString,QString> > >fromList;
                        QList<QList<QPair<QString,QString> > >toList;
                        child2Node = childNode.firstChild();
                        while (!child2Node.isNull())
                        {
                            if (child2Node.toElement().tagName() == "message") //message
                            {
                                if (child2Node.toElement().hasAttribute("protocol") &&
                                    child2Node.toElement().hasAttribute("message"))
                                {
                                    QString mes = child2Node.toElement().attributeNode("message").
                                                                         value().trimmed();
                                    if (!QRegExp("[a-zA-Z0-9_]+").exactMatch(mes))
                                    {
                                        errorText = QObject::tr("Указан некорректный код сообщения '")+
                                                    mes+QObject::tr("' в цели '")+targetName+"'.";
                                        return false;
                                    }
                                    messagesList << mes;
                                    QString protocol = child2Node.toElement().attributeNode("protocol").
                                                                              value().trimmed();
                                    if (!QRegExp("[a-zA-Z0-9_]+").exactMatch(protocol))
                                    {
                                        errorText = QObject::tr("Указан некорректный протокол сообщения '")+
                                                    protocol+QObject::tr("' в цели '")+targetName+"'.";
                                        return false;
                                    }
                                    protocolsList << protocol;

                                    QList<QPair<QString,QString> > oneTargetTo;
                                    if (child2Node.toElement().hasAttribute("to"))
                                    {
                                        QStringList listTo = child2Node.toElement().attributeNode("to").
                                                                                         value().split(",");
                                        for (int i=0;i<listTo.size();i++)
                                        {
                                            QStringList onePair = listTo.at(i).split("=");         
                                            if (onePair.size() == 2)
                                            {
                                                oneTargetTo.append(QPair<QString,QString> (onePair.at(0),onePair.at(1)));
                                            }
                                            else
                                            {
                                                oneTargetTo.append(QPair<QString,QString> ());
                                            }
                                        }
                                    }
                                    toList << oneTargetTo;

                                    QList<QPair<QString,QString> > oneTargetFrom;
                                    if (child2Node.toElement().hasAttribute("from"))
                                    {
                                        QStringList listFrom = child2Node.toElement().attributeNode("from").
                                                                                         value().split(",");
                                        for (int i=0;i<listFrom.size();i++)
                                        {
                                            QStringList onePair = listFrom.at(i).split("=");
                                            if (onePair.size() == 2)
                                            {
                                                oneTargetFrom.append(QPair<QString,QString> (onePair.at(0),onePair.at(1)));
                                            }
                                            else
                                            {
                                                oneTargetFrom.append(QPair<QString,QString> ());
                                            }
                                        }
                                    }
                                    fromList << oneTargetFrom;

                                    if (child2Node.toElement().hasAttribute("time"))
                                    {
                                        timesList << child2Node.toElement().toElement().attributeNode("time").
                                                                                        value().toInt();
                                    }
                                    else
                                    {
                                        timesList << 0;
                                    }
                                }
                                else
                                {
                                    if (!child2Node.toElement().hasAttribute("protocol"))
                                    {
                                        errorText = QObject::tr("Не указан протокол в сообщении цели '")+
                                                    targetName+"'.";
                                    }
                                    else if (!child2Node.toElement().hasAttribute("message"))
                                    {
                                        errorText = QObject::tr("Не указан код в сообщении цели '")+
                                                    targetName+"'.";
                                    }
                                    return false;
                                }
                            }
                            child2Node = child2Node.nextSibling();
                        }
                        messages[targetName] = messagesList;
                        protocols[targetName] = protocolsList;
                        times[targetName] = timesList;
                        targetsFrom[targetName] = fromList;
                        targetsTo[targetName] = toList;
                    }
                    else
                    {
                        errorText = QObject::tr("Не указано имя абонента цели.");
                        return false;
                    }
                }
                childNode = childNode.nextSibling();
            }
        }
        else if (node.toElement().tagName() == "outside") //outside
        {
            childNode = node.firstChild();
            while (!childNode.isNull())
            {
                if (childNode.toElement().tagName() == "message") //message outside
                {
                    if (childNode.toElement().hasAttribute("protocol"))
                    {
                        QString protocol = childNode.toElement().attributeNode("protocol").
                                                                 value().trimmed();
                        if (!QRegExp("[a-zA-Z0-9_]+").exactMatch(protocol))
                        {
                            errorText = QObject::tr("Указан некорректный протокол '")+protocol+
                                        QObject::tr("' ожидаемого сообщения.");
                            return false;
                        }
                        outsideProtocols.append(protocol);
                        if (childNode.toElement().hasAttribute("message"))
                        {
                            QString mes = childNode.toElement().attributeNode("message").
                                                                value().trimmed();
                            if (!QRegExp("[a-zA-Z0-9_]+").exactMatch(mes))
                            {
                                errorText = QObject::tr("Указан некорректный код '")+mes+
                                            QObject::tr("' ожидаемого сообщения.");
                                return false;
                            }
                            outsideMessages.append(mes);
                        }
                        else
                        {
                            errorText = QObject::tr("Не указан код в ожидаемом сообщении.");
                            return false;
                        }
                    }
                    else
                    {
                        errorText = QObject::tr("Не указан протокол в ожидаемом сообщении.");
                        return false;
                    }
                }
                childNode = childNode.nextSibling();
            }
        }
        node = node.nextSibling();
    }

    QList <PropertyDescription> listPropDecs = propertiesV2.values();
    QStringList properties = propertiesV2.keys();
    for (int i=0; i<listPropDecs.size(); i++)
    {
        for (int j=0; j<listPropDecs.at(i).lexemsUsedVars.size(); j++)
        {
            if (!properties.contains(listPropDecs.at(i).lexemsUsedVars.at(j)))
            {
                errorText = QObject::tr("Переменная eval:")+listPropDecs.at(i).lexemsUsedVars.at(j)
                            + QObject::tr(" не найдена.\n Компонент:") + name
                            + QObject::tr(" Свойство:") + properties.at(i);
                return false;
            }
        }
        for (int j=0; j<listPropDecs.at(i).valueUsedVars.size(); j++)
        {
            if (!properties.contains(listPropDecs.at(i).valueUsedVars.at(j)))
            {
                errorText = QObject::tr("Переменная value:")+listPropDecs.at(i).valueUsedVars.at(j)
                            + QObject::tr(" не найдена.\n Компонент:") + name
                            + QObject::tr(" Свойство:") + properties.at(i);
                return false;
            }
        }
    }

    for (int i=0; i<commandUsedVars.size(); i++)
    {        
        QStringList list = commandUsedVars.at(i);
        for (int j=0; j<list.size(); j++)
        {
            if (!properties.contains(list.at(j)))
            {
                errorText = QObject::tr("Переменная command:")+list.at(j)
                            + QObject::tr(" не найдена.\n Компонент:")+name;
                return false;
            }
        }
    }    

    //аппаратный журнал
    QMapIterator <QString,PropertyDescription> itr(propertiesV2);
    while (itr.hasNext())
    {
        itr.next();
        if (!itr.value().event.isEmpty())
        {
            hashDeviceListCategories[itr.value().alias].append(eventRulesAlias[itr.value().event]);
            hashCategoryListDevices[eventRulesAlias[itr.value().event]].append(itr.value().alias);
        }

        if (itr.key().contains("_bac"))
        {
            QString key = itr.key();
            key.chop(4);
            hashPropertyBacToDesription.insert(key,itr.value().alias);
        }
    }

    QMapIterator <QString,QMap<QString,QString> > _itr(eventRules);
    while (_itr.hasNext())
    {
        _itr.next();
        QMapIterator <QString,QString> itr2(_itr.value());
        while (itr2.hasNext())
        {
            itr2.next();
            if (!hashCategoryListAfters[_itr.key()].contains(itr2.value()))
            {
                hashCategoryListAfters[_itr.key()].append(itr2.value());
            }
            if (!hashAfterListCategories[itr2.value()].contains(_itr.key()))
            {
                hashAfterListCategories[itr2.value()].append(_itr.key());
            }
        }
        if (!hashCategoryListAfters[_itr.key()].contains(alterEventRule[_itr.key()]))
        {
            hashCategoryListAfters[_itr.key()].append(alterEventRule[_itr.key()]);
        }
        if (!hashAfterListCategories[alterEventRule[_itr.key()]].contains(_itr.key()))
        {
            hashAfterListCategories[alterEventRule[_itr.key()]].append(_itr.key());
        }
    }
    return Configuration::parseDocument(aRoot);
}



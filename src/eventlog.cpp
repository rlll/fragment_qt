#include "eventlog.h"

EventLog::EventLog(QObject *parent) : QObject(parent){}
EventLog::~EventLog(){}

EventLog::EventsTypes EventLog::getTypeByString(const QString & aString)
{
    QString string = aString.toLower();
    if (string == "usual")
    {
        return EventLog::USUAL;
    }
    else if (string == "fail")
    {
        return EventLog::FAIL;
    }
    else if (string == "fault")
    {
        return EventLog::FAULT;
    }
    else if (string == "norm")
    {
        return EventLog::ONORNORM;
    }
    else if (string == "off")
    {
        return EventLog::OFF;
    }
    else
    {
        return EventLog::UNKNOWN;
    }
}

int EventLog::getSecsByPeriod(int period)
{
    if (period == EventLog::WEEK)
    {
        return 604800;
    }
    else
        if (period == EventLog::DAY)
        {
            return 86400;
        }
    return 0;
}


#ifndef EVALUATION_H
#define EVALUATION_H

#include <QDebug>
#include <QMap>
#include <QMessageBox>
#include <QRegExp>
#include <QString>
#include <QStringList>

#include "global.h"

class Evaluation : public QObject
{
    Q_OBJECT
public:

    Evaluation(const QList <QStringList> aConditionLexems,
               const QStringList aUsedVars,
               const QStringList aConstStrings,
               QMap<QString, UserFunction> *aUserFunctions,
               QObject *parent = 0);
    Evaluation(const QString &, QStringList aNameUserFunctions, QObject *parent = 0);

    ~Evaluation();

    //Исходное выражение
    void setExpression(const QString &);
    QString getExpression();

    QStringList getUsedVars();
    QList <QStringList> getConditionLexems(){return conditionLexems;}
    QStringList getCS(){return constStrings;}

    //Вычисление в контексте переданных переменных
    QString evaluate(QHash<QString,QString>*);    
    QString evaluate(QStringList aReadyLexems, QHash<QString,QString> *aContext);
    QString evaluateT(int level, QHash<QString,QString>*);
    QString evaluateU(UserFunction aUF, QStringList aParameterValues, QHash<QString,QString> *aContext);

    static QString constIntPattern() {return "[-]?[0-9]+";}
    static QString constFloatPattern() {return "[-]?[0-9]+[.][0-9]+";}
    static QString constStringPattern() {return "'[^']*'";}
    static QString varPattern() {return "[A-Za-z0-9_]+";}

//protected:
    typedef QString (Evaluation::*Function)(const QStringList &);
    typedef QString (Evaluation::*Operation)(const QStringList &);

    QString deleteSpaces(QString);
    void ruleForBrackets(QString source);
    void ruleForNonAccountDelimiters(QString source);
    void preSplitOnLexems();
    void fullSplitOnLexems();
    QList <int> getPriorities(QStringList aLexems);
    void getCondTree(QList <QStringList> *aCondTree, QStringList aLexems, QList <int> aPriorities, int aStart = 0);
    QString getOrder(QString aFirstLexem, QStringList lexems);
    QStringList sort(QStringList indexes);
    QStringList preDelimiters; //Основные разделители
    QStringList preLexems;
    QString errorText;


    void initSintax();
    void splitOnLexems();
    QString patternByDelimiters(const QStringList &);
    QString lexemWithType(const QString &);

    QString operationAnd(const QStringList &);
    QString operationOr(const QStringList &);

    QString functionDep(const QStringList &);
    QString functionEqual(const QStringList &);
    QString functionNonEqual(const QStringList &);
    QString functionMult(const QStringList &);
    QString functionSum(const QStringList &);
    QString functionSwitch(const QStringList &);

    QString functionMessage(const QStringList &);    

    QString functionMore(const QStringList &);
    QString functionLess(const QStringList &);
    QString functionInRange(const QStringList & aParameters);
    QString functionMid(const QStringList & aParameters);
    QString functionMid64(const QStringList & aParameters);
    QString functionStrF(const QStringList & aParameters);

    QString functionMessageIf(const QStringList &);
    QString functionMod(const QStringList &);
    QString functionNot(const QStringList &);

    double toNum(QString str, bool *ok);

private:
    QString expression;

    QStringList operations; // Операции в порядке приоритета
    QList<Operation> operationList;
    QStringList functions;  // Функции
    QList<Function> functionList;        

    QStringList delimiters;              // Все разделители
    QStringList priorityUpDelimiters;    // Разделители, повышающие приоритет
    QStringList priorityDownDelimiters;  // Разделители, понижающие приоритет
    QStringList nonAccountDelimiters;    // Не учитываемые разделители
    QStringList separatorDelimiters;     // Выделяющие разделители
    QStringList operationDelimiters;     // Разделители операции
    QStringList functionDelimiters;      // Разделители функции
    QStringList nameUserFunctions;           // Пользовательские функции
    QStringList userFunctionDelimiters;  // Разделители пользовательские функции

    QStringList conditions;
    QString conditionIf;
    QStringList conditionDelimiters;
    QList <QStringList> conditionLexems;
    QMap<QString, UserFunction>* userFunctions;

    QStringList readyLexems;

    QString stringMark;              // Метка строки
    QString leftSeparatorDelimiter;  // Левый разделитель
    QString rightSeparatorDelimiter; // Правый разделитель

    QStringList usedVars;
    QStringList constStrings;

signals:
    void signalMessage(const QString &,const QString &);
};
#endif

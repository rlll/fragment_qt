#include "configuration.h"

Configuration::Configuration()
{
}

Configuration::~Configuration()
{
}

QString Configuration::lastError()
{
    return errorText;
}

bool Configuration::hasWarning()
{
    return warningFlag;
}

QString Configuration::lastWarning()
{
    return warningText;
}

QStringList Configuration::allWarnings()
{
    return allWarningTexts;
}

bool Configuration::parse(const QString & aXmlFile)
{
    clearData();

    xmlFile = aXmlFile;
    QFile file(xmlFile);

    if (!file.exists())
    {
        errorText = QObject::tr("Файл не существует.");
        return false;
    }

    QDomElement root;
    QDomDocument document;

    QString errorMsg;
    int errorLine;
    int errorColumn;

    if (!document.setContent(&file,&errorMsg,&errorLine,&errorColumn))
    {
        errorText = errorMsg + QObject::tr(" (строка: ") + QString("%1").arg(errorLine) +
                    QObject::tr(" столбец: ") + QString("%1").arg(errorColumn) + QObject::tr(")");
        return false;
    }

    root = document.documentElement();
    if (root.tagName() != "configuration")
    {
        errorText = QObject::tr("Корневой тег не <configuration>");
        return false;
    }

    if (root.hasAttribute("type"))
    {
        QString typeIn = root.attributeNode("type").value().trimmed();
        if (typeIn != type)
        {
            errorText = QObject::tr("Тип указанный в атрибуте type тега <configuration> не совпадает с ожидаемым ")+
                        QObject::tr("(указанный тип: ")+typeIn+QObject::tr(" ожидаемый: ")+type;
            return false;
        }
    }
    else
    {
        errorText = QObject::tr("Отсутствует атрибут type, указывающий тип конфигурации");
        return false;
    }
    warningFlag = false;
    return parseDocument(root);
}

//protected
void Configuration::clearData()
{
}

bool Configuration::parseDocument(QDomElement aRoot)
{
    return true;
}
